// Section 1
import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { FormModel } from '../Models/form.model';

// Section 2
export const ADD_FORM = '[FORM] Add';

// Section 3
export class AddForm implements Action {
  readonly type = ADD_FORM;

  constructor(public payload: FormModel) { }
}


// Section 4 export class
export type Actions = AddForm;
