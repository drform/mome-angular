// Section 1
import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';

// Section 2
export const ADD_SEARCH = '[SEARCH] Add';
export const REMOVE_SEARCH = '[SEARCH] Remove';

// Section 3
export class AddSearch implements Action {
  readonly type = ADD_SEARCH;

  constructor(public payload: any) { }
}

export class RemoveSearch implements Action {
  readonly type = REMOVE_SEARCH;

  constructor() { }
}

// Section 4 export class
export type Actions = AddSearch | RemoveSearch;
