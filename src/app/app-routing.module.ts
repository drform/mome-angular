import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignupComponent } from './login/signup/signup.component';
import { AuthGuard } from './Services/auth.guard';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: '/signin', pathMatch: 'full' },
      {
        path: 'signin',
        loadChildren: './login/signin/signin.module#SigninModule'
      },
      { path: 'signup', component: SignupComponent },
      {
        path: 'forgotpassword',
        loadChildren: './login/forgotpassword/forgotpassword.module#ForgotpasswordModule'
      },
      {
        path: 'settings',
        loadChildren: './settings/settings.module#SettingsModule',
      },
      {
        path: 'groups',
        loadChildren: './groups/groups.module#GroupsModule',
      },
      {
        path: 'credit',
        loadChildren: './layout/credit/credit.module#CreditModule'
      }
    ],

  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
