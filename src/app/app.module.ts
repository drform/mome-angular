import { BrowserModule } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { UserReducer } from './reducers/user.reducer';
import { FormReducer } from './reducers/form.reducer';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './services/auth.interceptor';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutModule } from './Layout/layout.module';

import { SignupComponent } from './login/signup/signup.component';
import { SearchReducer } from './reducers/search.reducer';

import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { RouterModule } from '@angular/router';

import { MatFormFieldModule } from '@angular/material/form-field';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DemoMaterialModule } from './material-module';

@NgModule({

  declarations: [
    AppComponent,
    SignupComponent
  ],
  imports: [
    MatFormFieldModule,
    BrowserAnimationsModule,
    DemoMaterialModule,
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    FormsModule,
    HttpClientModule,
    LayoutModule,

    SweetAlert2Module.forRoot(),
    StoreModule.forRoot({
      user: UserReducer,
      form: FormReducer,
      search: SearchReducer
    })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
