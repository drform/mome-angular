import { User } from './Models/user.model';
import { FormModel } from './models/form.model';

export interface AppState {
  readonly user: User;
}

export interface FormState {
  readonly form: FormModel;
}

export interface SearchState {
  readonly search: any;
}


