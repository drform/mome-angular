import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { FormService } from '../../../services/form.service';
import { FormQuestion } from '../../../models/question.model';
import { FormList } from '../../../models/form.model';
import { Host } from '../../../../environments/environment';

import { FormAdd, FormItem } from '../../../models/addform.model';
import { AlertService } from '../../../shares/alert.service';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { AlertImagesizeService } from '../../../shares/alert-imagesize.service';
@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  HOST_URL = Host.url;
  groupId = localStorage.getItem('groupId');
  groupName = localStorage.getItem('groupName');
  formName = localStorage.getItem('formName');
  formId = localStorage.getItem('form_id');
  myForm: FormList;
  addForm: FormAdd;
  formImage = [
    {
      url: '',
      type: ''
    }
  ];
  imageName = 'Change image';
  selectedFiles: File;

  questionData: Array<FormQuestion> = [];
  questionList;

  myItem: Array<FormItem> = [];
  formItemDelete = [];

  displayDropdown = 'none';
  dropdownValue = '';
  dropdownIndex;
  dropdownchoiceIndex;

  displayCheckbox = 'none';
  checkboxValue = '';
  checkboxIndex;
  checkboxchoiceIndex;

  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  isPreDate: any = [];
  isPreDatetime: any = [];
  isPreString: any = [];
  isPreText: any = [];
  isPreInt: any = [];
  isPreFloat: any = [];
  isPreCheck: any = [];
  isPreImage: any = [];
  isPreFile: any = [];
  isPreDropdown: any = [];
  imgTable = [];
  examImage = [];
  dropdownSelect = 'select';

  isRequired: any = [];

  isSubmitClicked = false;
  isLoading = false;

  constructor(
    private formService: FormService,
    private alertService: AlertService,
    private alertImage: AlertImagesizeService
  ) { }

  ngOnInit() {
    this.getForm();
    this.getAnswer();
  }

  getForm() {
    this.formImage = [];
    this.formService.getFormId(this.formId).subscribe(data => {
      this.myForm = data;
      this.formImage.push({
        url: this.HOST_URL + this.myForm.form.image,
        type: 'image/png'
      });

    });
  }

  getAnswer() {
    this.formService.getFormItem(this.formId).subscribe(data => {

      this.questionData.push(data);

      const len = Object.keys(this.questionData[0].item);
      this.questionList = len.length;

      for (let i = 0; i < this.questionList; i++) {

        const temp = [];
        if (this.questionData[0].item[i].data_type === 'Dropdown' || this.questionData[0].item[i].data_type === 'Checkbox') {
          for (const items of this.questionData[0].item[i].dropdown) {
            temp.push({
              id: items.id,
              data: items.dropdown_value
            });
          }
        }
        this.questionData[0].item[i].value = temp;
      }

      this.getAnswerToQuestion();
    });
  }

  getAnswerToQuestion() {
    const leng = Object.keys(this.questionData[0].item);
    this.questionData[0].item.forEach(element => {
      if (element.data_type === 'Dropdown') {
        this.myItem.push({
          id: element.id,
          order_no: element.order_no,
          data_type: element.data_type,
          title: element.title,
          description: element.description,
          required: element.required,
          av_search: element.av_search,
          value: element.value,
          is_dropdown: true,
          is_checkbox: false,
          is_newtype: false,
          is_datatype: 'Dropdown'
        });
      } else if (element.data_type === 'Checkbox') {
        this.myItem.push({
          id: element.id,
          order_no: element.order_no,
          data_type: element.data_type,
          title: element.title,
          description: element.description,
          required: element.required,
          av_search: element.av_search,
          value: element.value,
          is_dropdown: false,
          is_checkbox: true,
          is_newtype: false,
          is_datatype: 'Checkbox'
        });
      } else {
        let type: any = '';
        if (element.data_type === 'String') {
          type = 'Short Ans';
        } else if (element.data_type === 'Text') {
          type = 'Long Ans';
        } else if (element.data_type === 'Integer') {
          type = 'Number';
        } else if (element.data_type === 'Float') {
          type = 'Decimal';
        } else if (element.data_type === 'Datetime') {
          type = 'Time';
        } else if (element.data_type === 'Date') {
          type = 'Date';
        } else if (element.data_type === 'Image') {
          type = 'Image';
        } else if (element.data_type === 'File') {
          type = 'File';
        }
        this.myItem.push({
          id: element.id,
          order_no: element.order_no,
          data_type: element.data_type,
          title: element.title,
          description: element.description,
          required: element.required,
          av_search: element.av_search,
          value: element.value,
          is_dropdown: false,
          is_checkbox: false,
          is_newtype: false,
          is_datatype: type
        });
      }
    });

    this.myItem.forEach(item => {
      item.value.forEach(value => {
        value.is_removable = false;
      });
    });
  }

  addItem() {
    this.questionList += 1;
    if (this.questionList !== 0) {

      this.myItem.push({
        order_no: this.questionList,
        data_type: 'String',
        title: '',
        description: '',
        required: true,
        av_search: true,
        value: [],
        is_dropdown: false,
        is_checkbox: false,
        is_newtype: true,
      });

    }

  }

  removeItem(index) {
    if (this.questionList > 0) {

      if (!this.myItem[index].is_newtype) {
        this.formItemDelete.push({
          id: this.myItem[index].id
        });
      }

      this.questionList -= 1;
      this.myItem.splice(index, 1);

      for (let i = 0; i < this.myItem.length; i++) {
        this.myItem[i].order_no = i + 1;
      }
    }
  }

  drop(event: CdkDragDrop<string[]>) {

    moveItemInArray(this.myItem, event.previousIndex, event.currentIndex);
    for (let i = 0; i < this.myItem.length; i++) {
      this.myItem[i].order_no = i + 1;
    }
  }

  addItemDropdown(event, index) {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      this.myItem[index].value.push({
        data: value.trim(),
        is_removable: true
      });
    }

    if (input) {
      input.value = '';
    }
  }

  onOpenEditDropdown(index, indexDropdown) {
    this.dropdownIndex = index;
    this.dropdownchoiceIndex = indexDropdown;
    this.dropdownValue = this.myItem[index].value[indexDropdown].data;
    this.displayDropdown = 'block';
  }
  onCloseEditDropdown() {
    this.displayDropdown = 'none';
  }
  editDropdown() {
    this.myItem[this.dropdownIndex].value[this.dropdownchoiceIndex].data = this.dropdownValue;
    this.displayDropdown = 'none';
  }

  onOpenEditCheckbox(index, indexCheckbox) {
    this.checkboxIndex = index;
    this.checkboxchoiceIndex = indexCheckbox;
    this.checkboxValue = this.myItem[index].value[indexCheckbox].data;
    this.displayCheckbox = 'block';
  }
  onCloseEditCheckbox() {
    this.displayCheckbox = 'none';
  }
  editCheckbox() {
    this.myItem[this.checkboxIndex].value[this.checkboxchoiceIndex].data = this.checkboxValue;
    this.displayCheckbox = 'none';
  }

  removeItemDropdown(index, indexDropdown) {
    this.myItem[index].value.splice(indexDropdown, 1);
  }
  removeItemCheckbox(index, checkboxIndex) {
    this.myItem[index].value.splice(checkboxIndex, 1);
  }


  addItemCheckbox(event, index) {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      this.myItem[index].value.push({
        data: value.trim(),
        is_removable: true
      });
    }

    if (input) {
      input.value = '';
    }
  }

  handleFileInput(event) {
    this.formImage = [];
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < event.target.files.length; i++) {
      this.selectedFiles = event.target.files[i] as File;
      if (this.selectedFiles.size < 10 * 1024 * 1024) {
        const reader = new FileReader();
        reader.onload = (evented: any) => {
          this.formImage.push({
            url: evented.target.result,
            type: this.selectedFiles.type
          });
          this.imageName = this.selectedFiles.name;
        };
        reader.readAsDataURL(this.selectedFiles);
      } else {
        this.alertImage.alertImageOverSize();
      }

    }
  }

  selectType(typeItem, index) {
    if (typeItem === 'Date') {
      this.myItem[index].is_checkbox = false;
      this.myItem[index].is_dropdown = false;
      this.myItem[index].data_type = typeItem;

    } else if (typeItem === 'Datetime') {
      this.myItem[index].is_checkbox = false;
      this.myItem[index].is_dropdown = false;
      this.myItem[index].data_type = typeItem;

    } else if (typeItem === 'String') {
      this.myItem[index].is_checkbox = false;
      this.myItem[index].is_dropdown = false;
      this.myItem[index].data_type = typeItem;

    } else if (typeItem === 'Text') {
      this.myItem[index].is_checkbox = false;
      this.myItem[index].is_dropdown = false;
      this.myItem[index].data_type = typeItem;

    } else if (typeItem === 'Integer') {
      this.myItem[index].is_checkbox = false;
      this.myItem[index].is_dropdown = false;
      this.myItem[index].data_type = typeItem;

    } else if (typeItem === 'Float') {
      this.myItem[index].is_checkbox = false;
      this.myItem[index].is_dropdown = false;
      this.myItem[index].data_type = typeItem;

    } else if (typeItem === 'Checkbox') {
      this.myItem[index].value = [];
      this.myItem[index].is_checkbox = true;
      this.myItem[index].is_dropdown = false;
      this.myItem[index].data_type = typeItem;

    } else if (typeItem === 'Image') {
      this.myItem[index].is_checkbox = false;
      this.myItem[index].is_dropdown = false;
      this.myItem[index].data_type = typeItem;

    } else if (typeItem === 'File') {
      this.myItem[index].value = [];
      this.myItem[index].is_checkbox = false;
      this.myItem[index].is_dropdown = false;
      this.myItem[index].data_type = typeItem;

    } else if (typeItem === 'Dropdown') {
      this.myItem[index].value = [];
      this.myItem[index].is_checkbox = false;
      this.myItem[index].is_dropdown = true;
      this.myItem[index].data_type = typeItem;

    }
  }


  submit() {
    this.isSubmitClicked = true;
    this.isLoading = true;
    let imageText: any;
    if (this.formImage[0].type === 'image/jpeg') {
      imageText = this.formImage[0].url.split(',')[1];
    } else if (this.formImage[0].type === 'image/png') {
      imageText = this.formImage[0].url.split(',')[1];
    } else {
      imageText = '';
    }
    const imageUrl = imageText;

    this.addForm = {
      form: {
        title: this.myForm.form.title,
        description: this.myForm.form.description,
        image: imageUrl
      },
      form_item: this.myItem
    };

    console.log(this.addForm.form_item);
    let checkNotNone = true;
    for (let i = 0; i < this.addForm.form_item.length; i++) {
      if (this.addForm.form_item[i].data_type === 'Dropdown' || this.addForm.form_item[i].data_type === 'Checkbox') {
        if (this.myItem[i].value.length === 0) {
          checkNotNone = false;
          break;
        }
      }
      if (this.addForm.form_item[i].title === '') {
        checkNotNone = false;
        break;
      }
    }

    if (checkNotNone) {
      this.addForm.form_item.sort(function (a, b) {
        return a.is_newtype - b.is_newtype;
      });
      console.log(this.addForm.form_item);

      this.isSubmitClicked = false;
      this.isLoading = false;
      this.alertService.alertEditForm(this.formId, this.addForm, this.groupId, this.formItemDelete).then(data => {
        this.isSubmitClicked = false;
        this.isLoading = false;
      });
    } else {
      this.isSubmitClicked = false;
      this.isLoading = false;
      this.alertService.alertNull();
    }
  }

  preview() {
    for (let i = 0; i < this.questionList; i++) {

      const type = this.myItem[i].data_type;
      const orderId = this.myItem[i].order_no;
      if (type === 'Date') {
        this.isPreDate[orderId - 1] = true;
        this.isPreDatetime[orderId - 1] = false;
        this.isPreString[orderId - 1] = false;
        this.isPreText[orderId - 1] = false;
        this.isPreInt[orderId - 1] = false;
        this.isPreFloat[orderId - 1] = false;
        this.isPreCheck[orderId - 1] = false;
        this.isPreImage[orderId - 1] = false;
        this.isPreFile[orderId - 1] = false;
        this.isPreDropdown[orderId - 1] = false;

      } else if (type === 'Datetime') {
        this.isPreDatetime[orderId - 1] = true;
        this.isPreDate[orderId - 1] = false;
        this.isPreString[orderId - 1] = false;
        this.isPreText[orderId - 1] = false;
        this.isPreInt[orderId - 1] = false;
        this.isPreFloat[orderId - 1] = false;
        this.isPreCheck[orderId - 1] = false;
        this.isPreImage[orderId - 1] = false;
        this.isPreFile[orderId - 1] = false;
        this.isPreDropdown[orderId - 1] = false;

      } else if (type === 'String') {
        this.isPreString[orderId - 1] = true;
        this.isPreDatetime[orderId - 1] = false;
        this.isPreDate[orderId - 1] = false;
        this.isPreText[orderId - 1] = false;
        this.isPreInt[orderId - 1] = false;
        this.isPreFloat[orderId - 1] = false;
        this.isPreCheck[orderId - 1] = false;
        this.isPreImage[orderId - 1] = false;
        this.isPreFile[orderId - 1] = false;
        this.isPreDropdown[orderId - 1] = false;

      } else if (type === 'Text') {
        this.isPreText[orderId - 1] = true;
        this.isPreDatetime[orderId - 1] = false;
        this.isPreDate[orderId - 1] = false;
        this.isPreString[orderId - 1] = false;
        this.isPreInt[orderId - 1] = false;
        this.isPreFloat[orderId - 1] = false;
        this.isPreCheck[orderId - 1] = false;
        this.isPreImage[orderId - 1] = false;
        this.isPreFile[orderId - 1] = false;
        this.isPreDropdown[orderId - 1] = false;

      } else if (type === 'Integer') {
        this.isPreInt[orderId - 1] = true;
        this.isPreDatetime[orderId - 1] = false;
        this.isPreDate[orderId - 1] = false;
        this.isPreString[orderId - 1] = false;
        this.isPreText[orderId - 1] = false;
        this.isPreFloat[orderId - 1] = false;
        this.isPreCheck[orderId - 1] = false;
        this.isPreImage[orderId - 1] = false;
        this.isPreFile[orderId - 1] = false;
        this.isPreDropdown[orderId - 1] = false;

      } else if (type === 'Float') {
        this.isPreFloat[orderId - 1] = true;
        this.isPreDatetime[orderId - 1] = false;
        this.isPreDate[orderId - 1] = false;
        this.isPreString[orderId - 1] = false;
        this.isPreText[orderId - 1] = false;
        this.isPreInt[orderId - 1] = false;
        this.isPreCheck[orderId - 1] = false;
        this.isPreImage[orderId - 1] = false;
        this.isPreFile[orderId - 1] = false;
        this.isPreDropdown[orderId - 1] = false;

      } else if (type === 'Checkbox') {
        this.isPreCheck[orderId - 1] = true;
        this.isPreDatetime[orderId - 1] = false;
        this.isPreDate[orderId - 1] = false;
        this.isPreString[orderId - 1] = false;
        this.isPreText[orderId - 1] = false;
        this.isPreInt[orderId - 1] = false;
        this.isPreFloat[orderId - 1] = false;
        this.isPreImage[orderId - 1] = false;
        this.isPreFile[orderId - 1] = false;
        this.isPreDropdown[orderId - 1] = false;

      } else if (type === 'Image') {
        this.isPreImage[orderId - 1] = true;
        this.isPreFile[orderId - 1] = false;
        this.isPreDatetime[orderId - 1] = false;
        this.isPreDate[orderId - 1] = false;
        this.isPreString[orderId - 1] = false;
        this.isPreText[orderId - 1] = false;
        this.isPreInt[orderId - 1] = false;
        this.isPreFloat[orderId - 1] = false;
        this.isPreCheck[orderId - 1] = false;
        this.isPreDropdown[orderId - 1] = false;

      } else if (type === 'File') {
        this.isPreFile[orderId - 1] = true;
        this.isPreDatetime[orderId - 1] = false;
        this.isPreDate[orderId - 1] = false;
        this.isPreString[orderId - 1] = false;
        this.isPreText[orderId - 1] = false;
        this.isPreInt[orderId - 1] = false;
        this.isPreFloat[orderId - 1] = false;
        this.isPreImage[orderId - 1] = false;
        this.isPreCheck[orderId - 1] = false;
        this.isPreDropdown[orderId - 1] = false;

      } else if (type === 'Dropdown') {
        this.isPreDropdown[orderId - 1] = true;
        this.isPreDatetime[orderId - 1] = false;
        this.isPreDate[orderId - 1] = false;
        this.isPreString[orderId - 1] = false;
        this.isPreText[orderId - 1] = false;
        this.isPreInt[orderId - 1] = false;
        this.isPreFloat[orderId - 1] = false;
        this.isPreImage[orderId - 1] = false;
        this.isPreCheck[orderId - 1] = false;
        this.isPreFile[orderId - 1] = false;

      }

    }
    this.getRequire();
  }

  getRequire() {
    this.isRequired = [];
    for (let i = 0; i < this.questionList; i++) {
      const require = this.myItem[i].required;

      if (require === true) {
        this.isRequired[i] = true;
      }
    }
  }

  handleFileInputExam(event, index) {
    this.imgTable = [];
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < event.target.files.length; i++) {
      this.selectedFiles = event.target.files[i] as File;
      const reader = new FileReader();
      reader.onload = (evented: any) => {
        this.imgTable.push(evented.target.result);
        this.examImage[index] = this.imgTable;

      };
      reader.readAsDataURL(this.selectedFiles);
    }
  }

  selectDropdown(itemdata) {
    this.dropdownSelect = itemdata;
  }


  requiredAction(index) {
    this.myItem[index].required = !this.myItem[index].required;
  }

  avsearchAction(index) {
    this.myItem[index].av_search = !this.myItem[index].av_search;
  }


}
