import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditRoutingModule } from './edit-routing.module';
import { EditComponent } from '../edit/edit.component';
import { FormsModule } from '@angular/forms';
import { DemoMaterialModule } from '../../../material-module';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';

@NgModule({
  declarations: [
    EditComponent
  ],
  imports: [
    CommonModule,
    EditRoutingModule,
    FormsModule,
    DemoMaterialModule,
    NgxMaterialTimepickerModule
  ]
})
export class EditModule { }
