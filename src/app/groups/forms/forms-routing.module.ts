import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsComponent } from '../forms/forms.component';
import { ListComponent } from './list/list.component';
import { ShareComponent } from './share/share.component';

const routes: Routes = [
  {
    path: '',
    component: FormsComponent,
    children: [
      { path: '', component: ListComponent },
      { path: 'share', component: ShareComponent },
      {
        path: 'new',
        loadChildren: '../forms/new/new.module#NewModule'
      },
      {
        path: 'edit/:id',
        loadChildren: '../forms/edit/edit.module#EditModule'
      },
      {
        path: 'show/:id',
        loadChildren: '../forms/show/show.module#ShowModule'
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormsRoutingModule { }
