import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { NgSelect2Module } from 'ng-select2';

import { FormsRoutingModule } from './forms-routing.module';
import { FormsComponent } from './forms.component';
import { ListComponent } from './list/list.component';
import { ShareComponent } from './share/share.component';

import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { DemoMaterialModule } from '../../material-module';
import { ScrollingModule } from '@angular/cdk/scrolling';

import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { LOCALE_ID } from '@angular/core';
import localeTh from '@angular/common/locales/th';
import { registerLocaleData } from '@angular/common';
registerLocaleData(localeTh, 'th');

@NgModule({
  declarations: [
    FormsComponent,
    ListComponent,
    ShareComponent
  ],
  imports: [
    CommonModule,
    FormsRoutingModule,
    FormsModule,
    DragDropModule,
    NgSelect2Module,
    ReactiveFormsModule,
    DemoMaterialModule,
    ScrollingModule,
    NgxMaterialTimepickerModule
  ],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: 'th'
    }
  ],
  exports: [ShareComponent]
})
export class FormsModules { }
