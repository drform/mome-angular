import { Component, OnInit } from '@angular/core';
import { FormModel } from '../../../models/form.model';
import { ActivatedRoute } from '@angular/router';

import { FormService } from '../../../services/form.service';
import { Host } from '../../../../environments/environment';

import { Store } from '@ngrx/store';
import { FormState } from '../../../app.state';
import * as FormActions from '../../../actions/form.actions';
import { UserService } from '../../../services/user.service';
import { User } from '../../../models/user.model';
import { ShareUserInfo } from '../../../models/share.model';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})
export class ListComponent implements OnInit {
  HOST_URL = Host.url;
  forms: FormModel;
  formsArray: any = [];
  allForms: FormModel;
  user: User;
  isAdmin: boolean;
  shareRole: any;
  groupId = this.route.snapshot.paramMap.get('id');
  groupName = localStorage.getItem('groupName');
  limit = 12;
  offset = 0;
  shareRoleId = null;
  order = 'id ASC';
  keyword = '';
  isLoadForm = false;
  isEmptyAllForm = false;
  isEmptyForm = false;
  onSearch = false;
  Alllimit = 12;
  Alloffset = 0;
  AllshareRoleId = 3;
  Allorder = 'id ASC';
  Allkeyword = '';
  shareUser: ShareUserInfo;
  constructor(
    private route: ActivatedRoute,
    private formService: FormService,
    private store: Store<FormState>,
    private userservice: UserService,
  ) { }
  ngOnInit() {
    this.checkAdmin();
    localStorage.setItem('groupId', this.groupId);
  }
  checkAdmin() {
    this.userservice.getUser().subscribe(state => {
      this.user = state;
      if (this.user != null) {
        if (this.user.user_info.role === 'User Admin') {
          this.isAdmin = true;
          this.shareRole = '';
        } else {
          this.isAdmin = false;
          this.shareRole = 1;
        }
        this.getForm();
      } else {

      }
    });
  }
  checkRole(event?) {
    this.keyword = '';
    this.onSearch = false;
    this.formsArray = [];
    this.limit = 12;
    this.offset = 0;
    if (this.isAdmin) {
      if (event.index === 0) {
        this.shareRole = '';
      } else {
        this.shareRole = 1;
      }
    } else {
      if (event.index === 0) {
        this.shareRole = 1;
      } else {
        this.shareRole = 2;
      }
    }
    this.getForm();
  }
  getForm() {
    this.forms = null;
    if (this.keyword !== '') {
      this.formsArray = [];
      this.onSearch = true;
    } else if (this.keyword === '' && this.onSearch) {
      this.formsArray = [];
      this.onSearch = false;
    }
    this.isEmptyAllForm = false;
    this.isEmptyForm = false;
    const param = {
      limit: this.limit,
      offset: this.offset,
      share_role_id: this.shareRole,
      order: this.order,
      keyword: this.keyword,
      group_id: this.groupId
    };
    this.formService.getForm(param).subscribe(data => {
      if (data) {
        const len = Object.keys(data);
        if (len.length !== 0) {
          this.forms = data;
          for (let i = 0; i < len.length; i++) {
            this.formsArray.push(this.forms[i]);
          }
          if (!this.forms[0].error) {
            this.store.dispatch(new FormActions.AddForm(this.forms));
          }
        } else if (this.formsArray.length > 0) {
          this.isLoadForm = false;
          this.isEmptyAllForm = false;
          this.isEmptyForm = false;
        } else {
          this.isEmptyForm = true;
          this.isEmptyAllForm = true;
        }
        if (len.length >= 12) {
          this.isLoadForm = true;
        } else {
          this.isLoadForm = false;
        }
      } else {
        this.isEmptyForm = true;
        this.isEmptyAllForm = true;
      }

    });
  }
  loadForm() {
    this.offset += 12;
    this.getForm();
  }
  selectForm(index) {
    const formname = 'Form: ' + this.formsArray[index].title;
    localStorage.setItem('formName', formname);
  }
}

