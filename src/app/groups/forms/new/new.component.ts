import { Component, OnInit } from '@angular/core';

import { FormAdd, FormItem } from '../../../models/addform.model';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

import { AlertService } from '../../../shares/alert.service';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { AlertImagesizeService } from '../../../shares/alert-imagesize.service';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css']
})
export class NewComponent implements OnInit {

  groupId = localStorage.getItem('groupId');
  groupName = localStorage.getItem('groupName');
  myItem: Array<FormItem> = [];
  itemlist = 0;
  isDragdrop = false;

  myForm: FormAdd;
  formTitle: string;
  formDescription: string;
  formImage = [
    {
      url: '/assets/img/default_pic.png',
      type: 'image/png'
    }
  ];
  imageName = 'Choose image';
  selectedFiles: File;

  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  examImage = [];
  imgTable = [];
  isPreDate: any = [];
  isPreDatetime: any = [];
  isPreString: any = [];
  isPreText: any = [];
  isPreInt: any = [];
  isPreFloat: any = [];
  isPreCheck: any = [];
  isPreImage: any = [];
  isPreFile: any = [];
  isPreDropdown: any = [];
  dropdownSelect = 'select';

  isRequired: any = [];

  constructor(
    private alertService: AlertService,
    private alertImage: AlertImagesizeService
  ) { }

  ngOnInit() {
    this.addItem();
  }

  addItem() {
    this.itemlist += 1;
    if (this.itemlist !== 0) {
      this.isDragdrop = true;
      this.myItem.push({
        order_no: this.itemlist,
        data_type: 'String',
        title: '',
        description: '',
        required: true,
        av_search: true,
        value: [],
        is_dropdown: false,
        is_checkbox: false
      });
    }
  }
  removeItem(index) {
    if (this.itemlist > 0) {
      this.itemlist -= 1;
      this.myItem.splice(index, 1);
      for (let i = 0; i < this.myItem.length; i++) {
        this.myItem[i].order_no = i + 1;
      }
    }
  }

  handleFileInput(event) {
    this.formImage = [];
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < event.target.files.length; i++) {
      this.selectedFiles = event.target.files[i] as File;
      if (this.selectedFiles.size < 10 * 1024 * 1024) {
        const reader = new FileReader();
        reader.onload = (evented: any) => {
          this.formImage.push({
            url: evented.target.result,
            type: this.selectedFiles.type
          });
          this.imageName = this.selectedFiles.name;
        };
        reader.readAsDataURL(this.selectedFiles);
      } else {
        this.alertImage.alertImageOverSize();
        this.imageName = 'Choose image';
        this.formImage.push({
          url: '/assets/img/default_pic.png',
          type: 'image/png'
        });
      }
    }
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.myItem, event.previousIndex, event.currentIndex);
    for (let i = 0; i < this.myItem.length; i++) {
      this.myItem[i].order_no = i + 1;
    }
  }

  selectType(typeItem, index) {
    if (typeItem === 'Date') {
      this.myItem[index].is_checkbox = false;
      this.myItem[index].is_dropdown = false;
      this.myItem[index].data_type = typeItem;
    } else if (typeItem === 'Datetime') {
      this.myItem[index].is_checkbox = false;
      this.myItem[index].is_dropdown = false;
      this.myItem[index].data_type = typeItem;
    } else if (typeItem === 'String') {
      this.myItem[index].is_checkbox = false;
      this.myItem[index].is_dropdown = false;
      this.myItem[index].data_type = typeItem;
    } else if (typeItem === 'Text') {
      this.myItem[index].is_checkbox = false;
      this.myItem[index].is_dropdown = false;
      this.myItem[index].data_type = typeItem;
    } else if (typeItem === 'Integer') {
      this.myItem[index].is_checkbox = false;
      this.myItem[index].is_dropdown = false;
      this.myItem[index].data_type = typeItem;
    } else if (typeItem === 'Float') {
      this.myItem[index].is_checkbox = false;
      this.myItem[index].is_dropdown = false;
      this.myItem[index].data_type = typeItem;
    } else if (typeItem === 'Checkbox') {
      this.myItem[index].value = [];
      this.myItem[index].is_checkbox = true;
      this.myItem[index].is_dropdown = false;
      this.myItem[index].data_type = typeItem;
    } else if (typeItem === 'Image') {
      this.myItem[index].is_checkbox = false;
      this.myItem[index].is_dropdown = false;
      this.myItem[index].data_type = typeItem;
    } else if (typeItem === 'File') {
      this.myItem[index].is_checkbox = false;
      this.myItem[index].is_dropdown = false;
      this.myItem[index].data_type = typeItem;
    } else if (typeItem === 'Dropdown') {
      this.myItem[index].value = [];
      this.myItem[index].is_checkbox = false;
      this.myItem[index].is_dropdown = true;
      this.myItem[index].data_type = typeItem;
    }
  }

  createForm() {
    let imageText: any;
    if (this.formImage[0].type === 'image/jpeg') {
      imageText = this.formImage[0].url.split(',')[1];
    } else if (this.formImage[0].type === 'image/png') {
      imageText = this.formImage[0].url.split(',')[1];
    } else {
      imageText = '';
    }
    const imageUrl = imageText;

    this.myForm = {
      form: {
        title: this.formTitle,
        description: this.formDescription,
        group_id: this.groupId,
        image: imageUrl
      },
      form_item: this.myItem
    };

    let checkNotNone = true;
    if (this.myItem.length > 0) {
      for (let i = 0; i < this.myForm.form_item.length; i++) {

        if (this.myForm.form_item[i].data_type === 'Dropdown' || this.myForm.form_item[i].data_type === 'Checkbox') {
          if (this.myItem[i].value.length === 0) {
            checkNotNone = false;
            break;
          }
        }
        if (this.myForm.form_item[i].title === '') {
          checkNotNone = false;
          break;
        }
      }
    } else {
      checkNotNone = false;
    }

    if (checkNotNone) {
      this.alertService.alertCreateForm(this.myForm, this.groupId).then(data => {
      });
    } else {
      this.alertService.alertEmptyCreateForm();
    }
  }

  preview() {
    const length = this.myItem.length;
    for (let i = 0; i < length; i++) {
      const type = this.myItem[i].data_type;
      const orderId = this.myItem[i].order_no;
      if (type === 'Date') {
        this.isPreDate[orderId - 1] = true;
        this.isPreDatetime[orderId - 1] = false;
        this.isPreString[orderId - 1] = false;
        this.isPreText[orderId - 1] = false;
        this.isPreInt[orderId - 1] = false;
        this.isPreImage[orderId - 1] = false;
        this.isPreFloat[orderId - 1] = false;
        this.isPreCheck[orderId - 1] = false;
        this.isPreFile[orderId - 1] = false;
        this.isPreDropdown[orderId - 1] = false;

      } else if (type === 'Datetime') {
        this.isPreDatetime[orderId - 1] = true;
        this.isPreDate[orderId - 1] = false;
        this.isPreString[orderId - 1] = false;
        this.isPreText[orderId - 1] = false;
        this.isPreInt[orderId - 1] = false;
        this.isPreImage[orderId - 1] = false;
        this.isPreFloat[orderId - 1] = false;
        this.isPreCheck[orderId - 1] = false;
        this.isPreFile[orderId - 1] = false;
        this.isPreDropdown[orderId - 1] = false;

      } else if (type === 'String') {
        this.isPreString[orderId - 1] = true;
        this.isPreDatetime[orderId - 1] = false;
        this.isPreDate[orderId - 1] = false;
        this.isPreText[orderId - 1] = false;
        this.isPreInt[orderId - 1] = false;
        this.isPreImage[orderId - 1] = false;
        this.isPreFloat[orderId - 1] = false;
        this.isPreCheck[orderId - 1] = false;
        this.isPreFile[orderId - 1] = false;
        this.isPreDropdown[orderId - 1] = false;

      } else if (type === 'Text') {
        this.isPreText[orderId - 1] = true;
        this.isPreDatetime[orderId - 1] = false;
        this.isPreDate[orderId - 1] = false;
        this.isPreString[orderId - 1] = false;
        this.isPreInt[orderId - 1] = false;
        this.isPreImage[orderId - 1] = false;
        this.isPreFloat[orderId - 1] = false;
        this.isPreCheck[orderId - 1] = false;
        this.isPreFile[orderId - 1] = false;
        this.isPreDropdown[orderId - 1] = false;

      } else if (type === 'Integer') {
        this.isPreInt[orderId - 1] = true;
        this.isPreDatetime[orderId - 1] = false;
        this.isPreDate[orderId - 1] = false;
        this.isPreString[orderId - 1] = false;
        this.isPreText[orderId - 1] = false;
        this.isPreFloat[orderId - 1] = false;
        this.isPreCheck[orderId - 1] = false;
        this.isPreImage[orderId - 1] = false;
        this.isPreFile[orderId - 1] = false;
        this.isPreDropdown[orderId - 1] = false;

      } else if (type === 'Float') {
        this.isPreFloat[orderId - 1] = true;
        this.isPreDatetime[orderId - 1] = false;
        this.isPreDate[orderId - 1] = false;
        this.isPreString[orderId - 1] = false;
        this.isPreText[orderId - 1] = false;
        this.isPreInt[orderId - 1] = false;
        this.isPreImage[orderId - 1] = false;
        this.isPreCheck[orderId - 1] = false;
        this.isPreFile[orderId - 1] = false;
        this.isPreDropdown[orderId - 1] = false;

      } else if (type === 'Checkbox') {
        this.isPreCheck[orderId - 1] = true;
        this.isPreDatetime[orderId - 1] = false;
        this.isPreDate[orderId - 1] = false;
        this.isPreString[orderId - 1] = false;
        this.isPreText[orderId - 1] = false;
        this.isPreInt[orderId - 1] = false;
        this.isPreImage[orderId - 1] = false;
        this.isPreFloat[orderId - 1] = false;
        this.isPreFile[orderId - 1] = false;
        this.isPreDropdown[orderId - 1] = false;

      } else if (type === 'Image') {
        this.isPreImage[orderId - 1] = true;
        this.isPreFile[orderId - 1] = false;
        this.isPreDatetime[orderId - 1] = false;
        this.isPreDate[orderId - 1] = false;
        this.isPreString[orderId - 1] = false;
        this.isPreText[orderId - 1] = false;
        this.isPreInt[orderId - 1] = false;
        this.isPreFloat[orderId - 1] = false;
        this.isPreCheck[orderId - 1] = false;
        this.isPreDropdown[orderId - 1] = false;

      } else if (type === 'File') {
        this.isPreFile[orderId - 1] = true;
        this.isPreDatetime[orderId - 1] = false;
        this.isPreDate[orderId - 1] = false;
        this.isPreString[orderId - 1] = false;
        this.isPreText[orderId - 1] = false;
        this.isPreInt[orderId - 1] = false;
        this.isPreImage[orderId - 1] = false;
        this.isPreFloat[orderId - 1] = false;
        this.isPreCheck[orderId - 1] = false;
        this.isPreDropdown[orderId - 1] = false;

      } else if (type === 'Dropdown') {
        this.isPreDropdown[orderId - 1] = true;
        this.isPreDatetime[orderId - 1] = false;
        this.isPreDate[orderId - 1] = false;
        this.isPreString[orderId - 1] = false;
        this.isPreText[orderId - 1] = false;
        this.isPreInt[orderId - 1] = false;
        this.isPreImage[orderId - 1] = false;
        this.isPreFloat[orderId - 1] = false;
        this.isPreCheck[orderId - 1] = false;
        this.isPreFile[orderId - 1] = false;

      }

    }
    this.getRequire();
  }

  getRequire() {
    this.isRequired = [];
    const leng = this.myItem.length;
    for (let i = 0; i < leng; i++) {
      const require = this.myItem[i].required;
      if (require === true) {
        this.isRequired[i] = true;
      }
    }
  }

  handleFileImageInputExam(event, index) {
    this.imgTable = [];
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < event.target.files.length; i++) {
      this.selectedFiles = event.target.files[i] as File;
      const reader = new FileReader();
      reader.onload = (evented: any) => {
        this.imgTable.push(evented.target.result);
        this.examImage[index] = this.imgTable;

      };
      reader.readAsDataURL(this.selectedFiles);
    }
  }

  addItemDropdown(event, index) {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      this.myItem[index].value.push({
        data: value.trim()
      });
    }

    if (input) {
      input.value = '';
    }
  }

  removeItemDropdown(index, dropdownlistIndex) {
    this.myItem[index].value.splice(dropdownlistIndex, 1);
  }


  selectDropdown(itemdata) {
    this.dropdownSelect = itemdata;
  }

  addItemCheckbox(event, index) {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      this.myItem[index].value.push({
        data: value.trim()
      });
    }

    if (input) {
      input.value = '';
    }
  }

  removeItemCheckbox(index, checkboxIndex) {
    this.myItem[index].value.splice(checkboxIndex, 1);
  }

  requiredAction(index) {
    this.myItem[index].required = !this.myItem[index].required;
  }

  avsearchAction(index) {
    this.myItem[index].av_search = !this.myItem[index].av_search;
  }


}
