import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewRoutingModule } from './new-routing.module';
import { FormsModule } from '@angular/forms';
import { NewComponent } from '../new/new.component';
import { DemoMaterialModule } from '../../../material-module';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';

@NgModule({
  imports: [
    CommonModule,
    NewRoutingModule,
    FormsModule,
    DemoMaterialModule,
    NgxMaterialTimepickerModule
  ],
  declarations: [
    NewComponent
  ],
})
export class NewModule { }
