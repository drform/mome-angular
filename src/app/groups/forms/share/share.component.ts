import { Component, OnInit } from '@angular/core';
import { FormService } from '../../../services/form.service';
import { Host } from '../../../../environments/environment';

import { FormModel } from '../../../models/form.model';
import { UserService } from '../../../services/user.service';
import { User } from '../../../models/user.model';

@Component({
  selector: 'app-share',
  templateUrl: './share.component.html',
  styleUrls: ['./share.component.css']
})
export class ShareComponent implements OnInit {

  HOST_URL = Host.url;
  groupId = localStorage.getItem('groupId');
  shareForms: FormModel;
  formsArray: any = [];
  user: User;
  isAdmin: boolean;

  limit = 12;
  offset = 0;
  shareRoleId = 2;
  order = 'id ASC';
  keywords = '';

  isLoadForm = false;
  isEmptyForm = false;

  constructor(
    private formService: FormService,
    private userservice: UserService, ) { }

  ngOnInit() {
    this.getShareform();
  }

  checkAdmin() {
    this.userservice.getUser().subscribe(state => {
      this.user = state;
      if (this.user != null) {
        if (this.user.user_info.role === 'User Admin') {
          this.isAdmin = true;
        } else {
          this.isAdmin = false;
        }
      }
      this.getShareform();
    });
  }

  getShareform() {
    this.isEmptyForm = false;
    const param = {
      limit: this.limit,
      offset: this.offset,
      share_role_id: this.shareRoleId,
      group_id: this.groupId,
      order: this.order,
      keyword: this.keywords
    };

    this.formService.getForm(param).subscribe(data => {
      if (data) {
        const len = Object.keys(data);
        if (len.length !== 0) {
          this.shareForms = data;
        } else if (this.formsArray.length > 0) {

        } else {
          this.isEmptyForm = true;
        }

        if (len.length >= 12) {
          this.isLoadForm = true;
        } else {
          this.isLoadForm = false;
        }
      } else {
        this.isEmptyForm = true;
      }


    });
  }

  loadForm() {
    this.limit += 12;
    this.getShareform();
  }

  selectForm(index) {
    const formname = 'Share: ' + this.shareForms[index].title;
    localStorage.setItem('formName', formname);
  }

}
