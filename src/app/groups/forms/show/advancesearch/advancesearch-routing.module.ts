import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdvancesearchComponent } from '../advancesearch/advancesearch.component';

const routes: Routes = [
  { path: '', component: AdvancesearchComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdvancesearchRoutingModule { }
