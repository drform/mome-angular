import { Component, OnInit } from '@angular/core';
import { AnswerService } from '../../../../services/answer.service';
import { ActivatedRoute } from '@angular/router';
import { FormQuestionSearch, AnswerList } from '../../../../models/search.model';

import { Host } from '../../../../../environments/environment';
import { FormList } from '../../../../models/form.model';
import { SearchService } from '../../../../services/search.service';

import { Store } from '@ngrx/store';
import { SearchState } from '../../../../../app/app.state';
import * as ShareActions from '../../../../actions/search.actions';
import { formatDate } from '@angular/common';
import { AnswerClicked } from '../../../../models/answer.model';
import { DateAdapter } from '@angular/material';

@Component({
  selector: 'app-advancesearch',
  templateUrl: './advancesearch.component.html',
  styleUrls: ['./advancesearch.component.css']
})
export class AdvancesearchComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private answerService: AnswerService,
    private userservice: SearchService,
    private store: Store<SearchState>,
    private dateAdapter: DateAdapter<Date>,
  ) { }
  HOST_URL = Host.url;
  formData: FormList;
  formItem: FormQuestionSearch;
  answer: AnswerList;
  answerData: AnswerList;
  value = [];
  formId = this.route.snapshot.paramMap.get('id');
  groupId = localStorage.getItem('groupId');
  groupName = localStorage.getItem('groupName');
  formName = localStorage.getItem('formName');

  isAnswerData: boolean;
  isAnswerDetail: boolean;
  radioChk: boolean[] = [];
  radioChkValue: number[] = [];
  answerClicked: AnswerClicked;

  searchKeyword = '';
  searchDate = '';

  boxList = [];
  isTextbox = [];
  isNumberbox = [];
  isDate = [];
  isDatetime = [];
  isCheckbox = [];
  isDropdown = [];

  checkboxData = [];
  isCheckboxBoolean: boolean[] = [];
  checkboxList = 0;

  dropdownData = [];
  isDropdownBoolean: boolean[] = [];
  dropdownList = 0;

  storeData: any;

  isSearchData: boolean;

  endInt: any;
  endFloat: any;
  endDate: any;
  endDatetime: any;

  body: any = {
    user_form_item: []
  };
  dateLeft: any = [];
  timeLeft: any = [];
  dateRight: any = [];
  timeRight: any = [];

  isform = false;
  ngOnInit() {
    this.dateAdapter.setLocale('th-TH');
    this.getFormItems();
  }

  getFormItems() {
    this.formId = this.route.snapshot.paramMap.get('id');
    this.answerService.getFormItems(this.formId).subscribe(data => {
      this.formItem = data;
      this.getBoxList();
    });
  }

  getBoxList() {
    const leng = Object.keys(this.formItem.item);
    for (let i = 0; i < leng.length; i++) {
      if (this.formItem.item[i].data_type === 'String' || this.formItem.item[i].data_type === 'Text') {
        this.isTextbox[i] = true;
        this.isform = true;
      } else if (this.formItem.item[i].data_type === 'Integer' || this.formItem.item[i].data_type === 'Float') {
        this.isNumberbox[i] = true;
        this.isform = true;
      } else if (this.formItem.item[i].data_type === 'Date') {
        this.isDate[i] = true;
        this.isform = true;
      } else if (this.formItem.item[i].data_type === 'Datetime') {
        this.isDatetime[i] = true;
        this.isform = true;
      } else if (this.formItem.item[i].data_type === 'Checkbox') {
        this.isCheckbox[i] = true;
        this.isform = true;
      } else if (this.formItem.item[i].data_type === 'Dropdown') {
        this.isDropdown[i] = true;
        this.isform = true;
      }
    }
  }



  setValueDropdown(index, indexChoice) {
    this.isDropdownBoolean[indexChoice] = !this.isDropdownBoolean[indexChoice];

    if (this.isDropdownBoolean[indexChoice] === true) {
      this.dropdownList += 1;
      this.dropdownData.push(this.formItem.item[index].dropdown[indexChoice].id);
    } else if (this.isDropdownBoolean[indexChoice] === false) {
      if (this.dropdownList > 0) {
        let i = 0;
        this.dropdownData.forEach(element => {
          if (element === this.formItem.item[index].dropdown[indexChoice].id) {
            this.dropdownData.splice(i, 1);
          }
          i++;
        });
      }
    }
  }

  setValueCheckbox(index, indexChoice) {
    this.isCheckboxBoolean[indexChoice] = !this.isCheckboxBoolean[indexChoice];

    if (this.isCheckboxBoolean[indexChoice] === true) {
      this.checkboxList += 1;
      this.checkboxData.push(this.formItem.item[index].dropdown[indexChoice].id);
    } else if (this.isCheckboxBoolean[indexChoice] === false) {
      if (this.checkboxList > 0) {
        let i = 0;
        this.checkboxData.forEach(element => {
          if (element === this.formItem.item[index].dropdown[indexChoice].id) {
            this.checkboxData.splice(i, 1);
          }
          i++;
        });
      }
    }
  }


  search() {
    this.body.user_form_item = [];
    let canSearch = true;
    for (let i = 0; i < this.radioChk.length; i++) {
      if (this.radioChk[i]) {
        if (!this.formItem.item[i].value) {
          canSearch = false;
        }
        const temp = {
          form_item_id: this.formItem.item[i].id,
          value: this.formItem.item[i].value + '',
          type: this.formItem.item[i].data_type_id
        };
        if (this.formItem.item[i].data_type === 'Date') {
          if (this.endDate) {
            temp.value = formatDate(new Date(this.formItem.item[i].value.toString()), 'yyyy-MM-dd', 'en-US', '+0700') + 't' + formatDate(new Date(this.endDate.toString()), 'yyyy-MM-dd', 'en-US', '+0700');
          } else {
            temp.value = formatDate(new Date(this.formItem.item[i].value.toString()), 'yyyy-MM-dd', 'en-US', '+0700');
          }
        } else if (this.formItem.item[i].data_type === 'Datetime') {
          if (!this.dateLeft[i] || !this.timeLeft[i]) {
            canSearch = false;
          } else if (this.dateRight[i] && this.timeRight[i]) {
            temp.value = this.dateLeft[i] + this.timeLeft[i] + 't' + this.dateRight[i] + this.timeRight[i];
          } else {
            temp.value = this.dateLeft[i] + this.timeLeft[i];
          }

        } else if (this.formItem.item[i].data_type === 'Integer') {
          if (this.endInt) {
            temp.value = this.formItem.item[i].value + 't' + this.endInt;
          } else {
            temp.value = this.formItem.item[i].value;
          }

        } else if (this.formItem.item[i].data_type === 'Float') {
          if (this.endFloat) {
            temp.value = this.formItem.item[i].value + 't' + this.endFloat;
          } else {
            temp.value = this.formItem.item[i].value;
          }

        } else if (this.formItem.item[i].data_type === 'Checkbox') {
          let str = '';
          this.checkboxData.forEach(elementx => {
            str += (elementx.toString()) + ',';
          });
          temp.value = str;
        } else if (this.formItem.item[i].data_type === 'Dropdown') {
          let str = '';
          this.dropdownData.forEach(elementx => {
            str += (elementx.toString()) + ',';
          });
          temp.value = str;
        }
        if (this.formItem.item[i].av_search) {
          this.body.user_form_item.push(temp);
        }
      }
    }


    const leng = Object.keys(this.body.user_form_item);
    if (leng.length > 0) {
      this.isSearchData = true;
    } else {
      this.isSearchData = false;
    }
    if (leng.length !== 0) {
      this.answerService.search(this.body, this.searchKeyword).subscribe(data => {
        if (data !== null) {
          this.answer = data;
          if (this.answer.user_form) {
            this.isAnswerData = true;
            this.isAnswerDetail = false;
            this.store.dispatch(new ShareActions.AddSearch(this.answer));
          } else {
            this.isAnswerData = false;
            this.isAnswerDetail = false;
          }
        }
      });
    }
  }

  getValueSearch() {
    this.userservice.getSearch().subscribe(state => {
      this.storeData = state;
      if (state) {
        const leng = Object.keys(this.storeData.answer);
        if (leng.length > 0) {
          if (state) {
            if ('' + this.storeData.answer[0].form_id === this.formId) {
              this.answer = state;
            }
          }
        }
      }
    });
  }
  getAnswerDetail(index) {
    this.answerService.getAnswerDetail(this.formId, this.answer.user_form[index].id).subscribe(data => {
      this.answerClicked = data;
      this.isAnswerDetail = true;

    });
  }
  getFormid() {
    localStorage.setItem('form_id', this.formId);
  }
  radioChange(index) {
    this.radioChk[index] = !this.radioChk[index];
    if (this.radioChk[index] === false) {
      this.value[index] = '';
      this.radioChkValue[index] = null;

    } else {
      this.radioChkValue[index] = this.formItem.item[index].id;
    }
  }
  searchUser() {
    this.answerService.search(this.body, this.searchKeyword).subscribe(data => {
      this.answer = data;
      const lenge = Object.keys(this.answer).length;
      if (lenge !== 0) {
        this.isAnswerData = true;
        this.store.dispatch(new ShareActions.AddSearch(this.answer));
      }
    });
  }
  textPDF(index) {
    const linkSource = this.HOST_URL + this.answerClicked.data[index].answer[0].value;
    const downloadLink = document.createElement('a');
    const fileName = 'sample.pdf';

    downloadLink.href = linkSource;
    downloadLink.download = fileName;
    window.open(downloadLink.href);
    // downloadLink.click();
  }
  EndDateChangeLeft($event, index) {
    this.dateLeft[index] = formatDate(new Date($event.value.toString()), 'yyyy-MM-ddT', 'en-US', '+0700');
  }
  EndDateChangeRight($event, index) {
    this.dateRight[index] = formatDate(new Date($event.value.toString()), 'yyyy-MM-ddT', 'en-US', '+0700');
  }
  exportAsXLSX() {
    this.answerService.exportXLSX(this.formId, this.body).then(data => {
      const fileXLSX: any = data;
      const downloadLink = document.createElement('a');
      downloadLink.href = this.HOST_URL + fileXLSX;
      window.open(downloadLink.href);
    }
    );
  }
}
