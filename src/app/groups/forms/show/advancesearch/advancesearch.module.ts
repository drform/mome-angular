import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { AdvancesearchRoutingModule } from './advancesearch-routing.module';
import { AdvancesearchComponent } from '../advancesearch/advancesearch.component';
import { DemoMaterialModule } from '../../../../material-module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AdvancesearchComponent
  ],
  imports: [
    CommonModule,
    AdvancesearchRoutingModule,
    DemoMaterialModule,
    FormsModule,
    NgxMaterialTimepickerModule
  ]
})
export class AdvancesearchModule { }
