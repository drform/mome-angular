import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Host } from '../../../../../environments/environment';
import { FormService } from '../../../../services/form.service';
import { AnswerModel } from '../../../../models/answer.model';
import { Router } from '@angular/router';
import { FormQuestion, Dropdown, Checkbox } from '../../../../models/question.model';

import { AnswerService } from '../../../../services/answer.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AlertService } from '../../../../shares/alert.service';
import { AlertRequireService } from '../../../../shares/alert-require.service';
import { HttpClient } from '@angular/common/http';
import { formatDate } from '@angular/common';
import { DateAdapter } from '@angular/material';
@Component({
  selector: 'app-answer',
  templateUrl: './answer.component.html',
  styleUrls: ['./answer.component.css']
})
export class AnswerComponent implements OnInit {

  requireData = {};
  groupId = localStorage.getItem('groupId');
  groupName = localStorage.getItem('groupName');
  formName = localStorage.getItem('formName');
  LIB: any;
  HOST_URL = Host.url;
  questionData: FormQuestion;
  dropdownData: Dropdown;
  checkboxData: Array<Checkbox> = [];
  checkData: Checkbox;
  checkArray = [];
  formId: any;


  imageData: any = [];
  imageShow: any = [];
  imgTable = [];

  fileData: FormGroup;
  fileValue: any = [];

  isDate: any = [];
  isDatetime: any = [];
  isString: any = [];
  isText: any = [];
  isInt: any = [];
  isFloat: any = [];
  isCheck: any = [];
  isImage: any = [];
  isFile: any = [];
  isDropdown: any = [];
  selectDropdown = [];

  isRequired: any = [];

  answer: AnswerModel;
  answerItem = [];
  examImage = [];
  selectedFiles: File;

  isCheckboxBoolean: boolean[] = [];
  checkboxList = 0;

  isSubmitClicked = false;
  isLoading = false;
  isValueEmpty = false;

  testValue: any;
  formData: FormData = new FormData();
  afuConfig: any;


  dateOfDatetime: any = [];
  timeOfDatetime: any = [];
  timePicker: any = [];
  constructor(
    private routes: Router,
    private route: ActivatedRoute,
    private formService: FormService,
    private answerService: AnswerService,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private http: HttpClient,
    private alertRequire: AlertRequireService,
    private dateAdapter: DateAdapter<Date>,

  ) { }

  ngOnInit() {
    this.dateAdapter.setLocale('th-TH');
    this.getForm();

    this.fileData = this.formBuilder.group({
      value: ['']
    });

    this.formData.delete('user_form_item[][order_no]');
    this.formData.delete('user_form_item[][value]');
    this.formData.delete('user_form_item[][value][]');
    this.formData.delete('user_form_item[][boolean]');

  }

  getForm() {
    this.formId = this.route.snapshot.paramMap.get('id');


    this.formService.getFormItem(this.formId).subscribe(data => {
      this.questionData = data;


      const leng = Object.keys(this.questionData.item);

      for (let i = 0; i < leng.length; i++) {
        if (this.questionData.item[i].data_type === 'Checkbox') {

          for (let j = 0; j < this.questionData.item[i].dropdown.length; j++) {
            this.questionData.item[i].dropdown[j].boolean = false;
            this.isCheckboxBoolean[j] = false;
            this.checkboxData.push({
              order_no: this.questionData.item[i].order_no,
              value: this.questionData.item[i].dropdown[j].id,
              dropdown_value: this.questionData.item[i].dropdown[j].dropdown_value,
              boolean: false
            });
          }
        }
      }


      this.getType();
      this.getRequire();
    });
  }
  EndDateChange($event, index) {
    this.dateOfDatetime[index] = formatDate(new Date($event.value.toString()), 'yyyy-MM-ddT', 'en-US', '+0700');
  }
  getType() {
    const leng = Object.keys(this.questionData.item);
    for (let i = 0; i < leng.length; i++) {
      const type = this.questionData.item[i].data_type;

      if (type === 'Date') {
        this.isDate[i] = true;
      } else if (type === 'Datetime') {
        this.isDatetime[i] = true;
      } else if (type === 'String') {
        this.isString[i] = true;
      } else if (type === 'Text') {
        this.isText[i] = true;
      } else if (type === 'Integer') {
        this.isInt[i] = true;
      } else if (type === 'Float') {
        this.isFloat[i] = true;
      } else if (type === 'Checkbox') {
        this.isCheck[i] = true;
      } else if (type === 'Image') {
        this.isImage[i] = true;
      } else if (type === 'File') {
        this.isFile[i] = true;
      } else if (type === 'Dropdown') {
        this.isDropdown[i] = true;
        this.selectDropdown[i] = 'select';
      }
    }

  }

  getRequire() {
    this.isRequired = [];
    const leng = Object.keys(this.questionData.item);
    for (let i = 0; i < leng.length; i++) {
      const require = this.questionData.item[i].required;

      if (require === true) {
        this.isRequired[i] = true;
      }
    }
  }

  handleFileInputExam(event, index) {
    this.imgTable = [];
    const img = [];
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < event.target.files.length; i++) {
      this.selectedFiles = event.target.files[i] as File;
      img.push(this.selectedFiles);

      const reader = new FileReader();
      reader.onload = (evented: any) => {
        this.imgTable.push(evented.target.result);
        this.imageShow[index] = this.imgTable;

      };
      reader.readAsDataURL(this.selectedFiles);
    }
    this.imageData[index] = img;
  }

  handleFile(event, index) {
    const file = event.target.files[0];
    this.fileValue[index] = file;

  }

  submit() {
    this.isSubmitClicked = true;
    this.isLoading = true;
    this.answerItem = [];
    this.formData.delete('user_form_item[][order_no]');
    this.formData.delete('user_form_item[][value]');
    this.formData.delete('user_form_item[][value][]');
    this.formData.delete('user_form_item[][boolean]');
    let index = 0;
    let isRequire = false;



    this.questionData.item.forEach(element => {
      if (element.required) {
        if (element.data_type === 'Checkbox') {
          const countCheckbox = element.dropdown.length;
          let number = 0;
          element.dropdown.forEach(drop => {
            if (!drop.boolean) {
              number++;
            }
            this.answerItem.push({
              order_no: (element.order_no).toString(),
              value: drop.id.toString(),
              boolean: drop.boolean.toString()
            });
          });
          if (number === countCheckbox) {
            isRequire = true;
          }
        } else if (element.data_type === 'Date') {
          if (element.value) {
            this.answerItem.push({
              order_no: (element.order_no).toString(),
              value: formatDate(new Date(element.value.toString()), 'yyyy-MM-dd', 'en-US', '+0700'),
              boolean: false
            });
          } else {
            isRequire = true;
          }

        } else if (element.data_type === 'Datetime') {
          if (this.dateOfDatetime[index] && this.timeOfDatetime[index]) {
            this.answerItem.push({
              order_no: (element.order_no).toString(),
              value: this.dateOfDatetime[index] + this.timeOfDatetime[index],
              boolean: false
            });
          } else {
            isRequire = true;
          }

        } else if (element.data_type === 'Image') {

          if (this.imageData.length > 0) {
            this.formData.append('user_form_item[][order_no]', element.order_no.toString());
            if (this.imageData[index]) {
              this.imageData[index].forEach(img => {
                if (img === '') {
                  isRequire = true;
                }
                this.formData.append('user_form_item[][value][]', img);

              });
            } else {
              isRequire = true;
            }

          } else {
            isRequire = true;
          }

        } else if (element.data_type === 'File') {
          if (this.fileValue.length > 0) {
            this.formData.append('user_form_item[][order_no]', (element.order_no).toString());
            this.formData.append('user_form_item[][value]', this.fileValue[index]);
          }

        } else {
          if (!element.value) {
            isRequire = true;
          } else {
            this.answerItem.push({
              order_no: (element.order_no).toString(),
              value: element.value.toString(),
              boolean: false
            });
          }

        }
      } else {
        if (element.data_type === 'Checkbox') {
          element.dropdown.forEach(drop => {
            this.answerItem.push({
              order_no: (element.order_no).toString(),
              value: drop.id.toString(),
              boolean: drop.boolean.toString()
            });
          });

        } else if (element.data_type === 'Date') {
          if (element.value) {
            this.answerItem.push({
              order_no: (element.order_no).toString(),
              value: formatDate(new Date(element.value.toString()), 'yyyy-MM-dd', 'en-US', '+0700'),
              boolean: false
            });
          } else {
            this.answerItem.push({
              order_no: (element.order_no).toString(),
              value: '',
              boolean: false
            });
          }

        } else if (element.data_type === 'Datetime') {
          this.answerItem.push({
            order_no: (element.order_no).toString(),
            value: this.dateOfDatetime[index] + this.timeOfDatetime[index],
            boolean: false
          });
        } else if (element.data_type === 'Image') {
          if (this.imageData.length > 0) {
            this.formData.append('user_form_item[][order_no]', element.order_no.toString());
            if (this.imageData[index]) {
              this.imageData[index].forEach(img => {
                this.formData.append('user_form_item[][value][]', img);
              });
            }

          }

        } else if (element.data_type === 'File') {
          if (this.fileValue.length > 0) {
            this.formData.append('user_form_item[][order_no]', (element.order_no).toString());
            this.formData.append('user_form_item[][value]', this.fileValue[index]);
          } else {
            if (element.value) {
              this.answerItem.push({
                order_no: (element.order_no).toString(),
                value: element.value.toString(),
                boolean: false
              });
            }
          }
        } else {
          if (element.value) {
            this.answerItem.push({
              order_no: (element.order_no).toString(),
              value: element.value.toString(),
              boolean: false
            });
          } else {
            this.answerItem.push({
              order_no: (element.order_no).toString(),
              value: '',
              boolean: false
            });
          }


        }
      }

      index++;
    });

    this.answer = {
      user_form_item: this.answerItem
    };
    console.log(this.answer);
    this.answer.user_form_item.forEach(element => {
      this.formData.append('user_form_item[][order_no]', (element.order_no).toString());
      this.formData.append('user_form_item[][value]', element.value.toString());
      this.formData.append('user_form_item[][boolean]', element.boolean.toString());
    });


    if (!isRequire) {
      this.alertService.alertMakeQuestion(this.formData, this.groupId, this.formId).then(data => {
        this.isSubmitClicked = false;
        this.isLoading = false;
      });
    } else {
      this.alertRequire.alertRequire();
      this.isSubmitClicked = false;
      this.isLoading = false;
    }
    isRequire = false;

  }


  dropdownSelect(item, index, indexDropdown) {
    this.selectDropdown[index] = item;
    this.questionData.item[index].value = this.questionData.item[index].dropdown[indexDropdown].id;
  }

  checkboxSelect(item, order, index, indexCheckbox) {
    this.questionData.item[index].dropdown[indexCheckbox].boolean = !this.questionData.item[index].dropdown[indexCheckbox].boolean;

  }
}
