import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AnswerRoutingModule } from './answer-routing.module';
import { AnswerComponent } from '../answer/answer.component';
import { FormsModule } from '@angular/forms';
import { DemoMaterialModule } from '../../../../material-module';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';

@NgModule({
  declarations: [
    AnswerComponent
  ],
  imports: [
    CommonModule,
    AnswerRoutingModule,
    FormsModule,
    DemoMaterialModule,
    NgxMaterialTimepickerModule
  ]
})
export class AnswerModule { }
