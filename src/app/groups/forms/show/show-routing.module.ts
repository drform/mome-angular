import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShowComponent } from '../show/show.component';
import { ShowinfoComponent } from './showinfo/showinfo.component';
const routes: Routes = [
  {
    path: '',
    component: ShowComponent
  },
  {
    path: 'answer',
    loadChildren: '../show/answer/answer.module#AnswerModule'
  },
  {
    path: 'advancesearch',
    loadChildren: '../show/advancesearch/advancesearch.module#AdvancesearchModule'
  },
  { path: 'info', component: ShowinfoComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShowRoutingModule { }
