import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { UserService } from '../../../services/user.service';
import { FormService } from '../../../services/form.service';
import { ShareService } from '../../../services/share.service';
import { AnswerService } from '../../../services/answer.service';
import { Host } from '../../../../environments/environment';
import { FormList } from '../../../models/form.model';
import { AnswerList } from '../../../models/answerlist.model';
import { ShareUserInfo } from '../../../models/share.model';
import { User, UserList } from '../../../models/user.model';
import { AlertService } from '../../../shares/alert.service';
import { DetailService } from '../../../services/detail/detail.service';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead/typeahead-match.class';
import { formatDate } from '@angular/common';
import { ShowAnswer } from '../../../models/showanswer.model';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AnswerModel } from '../../../models/answer.model';
import { AlerteditanswerService } from '../../../shares/alerteditanswer.service';
import { FormQuestion, Dropdown, Checkbox } from '../../../models/question.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertRequireService } from '../../../shares/alert-require.service';
import { DateAdapter } from '@angular/material';

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css']
})
export class ShowComponent implements OnInit {

  HOST_URL = Host.url;
  user: User;
  isOwner: boolean;
  isOwnerAnswer: boolean;
  allUsers: UserList;
  formData: FormList;
  answerData: AnswerList;
  isAnswerData: boolean;
  groupId = localStorage.getItem('groupId');
  groupName = localStorage.getItem('groupName');
  formId = this.route.snapshot.paramMap.get('id');
  formName = localStorage.getItem('formName');
  shareUser: Array<ShareUserInfo> = [];

  Users = [];
  username: any;
  selectedOption: any;

  saveData = [];

  isBackForm = false;
  isNextForm = false;
  stackPage = 0;

  searchKeyword = '';
  searchDateStart = '';
  searchDateEnd = '';
  limit = 5;
  offset = 0;

  isSelectAnswer;
  myAnswer: ShowAnswer;
  answerId;
  isDate = [];
  isDatetime = [];
  isFile = [];
  isImage = [];
  isCheckbox = [];
  valueCheckbox = [];

  questionData: FormQuestion;
  checkboxData: Array<Checkbox> = [];
  isRequired: any = [];

  isAnsDate: any = [];
  isAnsDatetime: any = [];
  isAnsString: any = [];
  isAnsText: any = [];
  isAnsInt: any = [];
  isAnsFloat: any = [];
  isAnsCheck: any = [];
  isAnsImage: any = [];
  isAnsFile: any = [];
  isAnsDropdown: any = [];
  selectAnsDropdown = [];

  imageShow: any = [];
  fileData: FormGroup;
  fileValue: any = [];
  fileName: any;
  answerItem = [];
  resultNumber: any = 0;

  timeOfDatetime: any = [];

  constructor(
    private routes: Router,
    private route: ActivatedRoute,
    private formService: FormService,
    private shareService: ShareService,
    private userservice: UserService,
    private alertService: AlertService,
    private detailService: DetailService,
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private dateAdapter: DateAdapter<Date>
  ) { }
  ngOnInit() {
    this.dateAdapter.setLocale('th-TH');
    this.fileData = this.formBuilder.group({
      name: [''],
      value: ['']
    });
    this.getFormId();
    this.getShareUser();

    if (JSON.parse(localStorage.getItem('keywords'))) {
      this.searchAnswerFormAll();
    } else {
      this.getAnswerFormAll();
      localStorage.removeItem('keywords');
    }
  }

  checkOwner() {
    this.userservice.getUser().subscribe(state => {
      this.user = state;
      if (this.user != null) {
        if (this.user.user_info.id === this.formData.form.user_id) {
          this.isOwner = true;
        } else if (this.user.user_info.role === 'User Admin') {
          this.isOwner = true;
        } else {
          this.isOwner = false;
        }
      }


    });
  }

  getFormId() {
    this.formService.getFormId(this.formId).subscribe(data => {
      localStorage.setItem('form_id', this.formId);

      this.formData = data;
      this.checkOwner();
    });
  }

  getDetail(index) {
    this.answerId = this.answerData.user_form[index].id.toString();
    this.getAnswer();
    localStorage.setItem('answerId', this.answerId);
    this.detailService.update(this.answerData.user_form[index].id);
    this.isSelectAnswer = true;

  }

  getOwnerAnswer() {
    this.userservice.getUser().subscribe(state => {
      this.user = state;

      if (this.user != null) {
        if (this.user.user_info.id === this.myAnswer.creater.user_id) {
          this.isOwnerAnswer = true;
        } else if (this.user.user_info.role === 'User Admin') {
          this.isOwnerAnswer = true;
        } else {
          this.isOwnerAnswer = false;
        }
      }


    });
  }



  getAnswerFormAll() {
    this.myAnswer = null;
    let param;
    if (this.searchDateStart && this.searchDateEnd) {
      param = {
        keyword: this.searchKeyword,
        date: this.searchDateStart + ',' + this.searchDateEnd,
        limit: this.limit,
        offset: this.offset
      };
    } else if (this.searchDateStart) {
      param = {
        keyword: this.searchKeyword,
        date: this.searchDateStart,
        limit: this.limit,
        offset: this.offset
      };
    } else {
      param = {
        keyword: this.searchKeyword,
        limit: this.limit,
        offset: this.offset
      };
    }

    this.formService.getAnswerFormAll(this.formId, param).subscribe(data => {
      this.answerData = data;

      if (this.answerData.user_form) {
        const leng = Object.keys(this.answerData.user_form);

        if (leng.length > 0) {
          this.isAnswerData = true;
          this.resultNumber = this.answerData.amount;
        } else {
          this.isAnswerData = false;
        }

        if (leng.length >= 5) {
          this.isNextForm = true;
        } else {
          this.isNextForm = false;
        }
        if (this.stackPage === 0) {
          this.isBackForm = false;
        } else {
          this.isBackForm = true;
        }
      }

    });
  }
  backPage() {
    this.stackPage -= 1;
    this.offset -= 5;
    this.myAnswer = null;
    this.getAnswerFormAll();
  }

  nextPage() {
    this.isBackForm = true;
    this.stackPage += 1;
    this.offset += 5;
    this.myAnswer = null;
    this.getAnswerFormAll();
  }

  getShareUser() {
    this.shareUser = [];
    this.shareService.getShareuser(this.formId).subscribe(data => {
      this.shareUser.push(data);
    });
  }
  searchAllUsers() {
    if (this.username !== '') {
      this.Users = [];
      this.userservice.searchUsers(this.username).subscribe(data => {
        this.allUsers = data;
        console.log(data);
        this.allUsers.users.forEach(user => {
          this.Users.push({
            username: user.username
          });
        });
        console.log(this.allUsers);
      });

    }
  }
  onSelect(event: TypeaheadMatch): void {
    this.selectedOption = event.item;
  }
  addShareform() {
    Swal.fire({
      title: 'Are you sure add user?',
      text: 'Confirm add this user.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3885DE',
      cancelButtonColor: '#e41c44',
      confirmButtonText: 'Yes',
    }).then((result) => {
      if (result.value) {
        this.shareService.addShareuser(this.username, this.formId).subscribe(data => {
          this.saveData.push(data);
          if (this.saveData[0].message === 'Share Successfully.') {
            Swal.fire({
              title: 'Add user Successed!',
              text: 'Your add user successed.',
              type: 'success',
            });
            this.saveData = [];
            this.username = '';
            this.getShareUser();
          } else {
            Swal.fire({
              title: this.saveData[0].error + '',
              text: 'Can not add this user',
              type: 'error',
              confirmButtonColor: '#3885DE',
              confirmButtonText: 'OK'
            });
            this.saveData = [];
            this.getShareUser();
          }
          this.selectedOption = null;
        });
      }
    });
  }
  deleteShareUser(userId) {
    Swal.fire({
      title: 'Are you sure delete user?',
      text: 'Confirm delete this user.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3885DE',
      cancelButtonColor: '#e41c44',
      confirmButtonText: 'Yes',
    }).then((result) => {
      if (result.value) {
        this.shareService.delShareuser(this.formId, userId).subscribe(data => {
          this.saveData.push(data);
          if (this.saveData[0].message === 'Delete Successfully.') {
            Swal.fire({
              title: 'Delete Successed!',
              text: 'Your delete user successed.',
              type: 'success',
            });
            this.saveData = [];
            this.getShareUser();
          } else {
            Swal.fire({
              title: this.saveData[0].error + '',
              text: 'Can not delete this user',
              type: 'error',
              confirmButtonColor: '#3885DE',
              confirmButtonText: 'OK'
            });
            this.saveData = [];
            this.getShareUser();
          }
          this.selectedOption = null;
        });
      }
    });
  }
  searchAnswerFormAll() {

    localStorage.removeItem('keywords');
  }

  deleteForm() {
    this.alertService.alertDeleteForm(this.formId, this.groupId);
  }

  editForm() {
    localStorage.setItem('form_id', this.formId);
  }




  showInfo() {
    localStorage.setItem('form_id', this.formId);
    localStorage.setItem('formName', this.formName);
  }
  getAnswer() {
    this.formService.getAnswerForm(this.formId, this.answerId).subscribe(data => {
      this.myAnswer = data;
      if (this.myAnswer.error) {

      } else {
        this.getOwnerAnswer();
        this.checkType();
        this.getQuestion();
      }
    });

  }

  checkType() {
    for (const ans of this.myAnswer.data) {
      const order = ans.order_no;
      if (ans.data_type_id === 1) {

        this.isDate[order - 1] = true;

      }
      if (ans.data_type_id === 2) {
        this.isDatetime[order - 1] = true;

      }
      if (ans.data_type_id === 7) {
        this.isCheckbox[order - 1] = true;

      }
      if (ans.data_type_id === 8) {
        this.isFile[order - 1] = true;

      }
      if (ans.data_type_id === 10) {
        this.isImage[order - 1] = true;
      }
    }
  }

  deleteAnswer() {
    const ansId = localStorage.getItem('answerId');
    this.alertService.alertDeleteAnswer(this.formId, this.groupId, this.answerId).then((resolve: any) => {
      if (resolve.value) {
        this.formService.deleteAnswer(this.formId, ansId).subscribe(data => {
          this.saveData.push(data);

          if (this.saveData[0].message === 'Delete Successfully.') {
            Swal.fire({
              title: 'Delete Successed!',
              text: 'Your delete answer successed.',
              type: 'success',
            });
            this.myAnswer = null;
            this.saveData = [];
            this.ngOnInit();

          } else {
            Swal.fire({
              title: 'Something wrong!',
              text: 'Can not delete this answer',
              type: 'error',
              confirmButtonColor: '#3885DE',
              confirmButtonText: 'OK'
            });
          }
        });
      }
    });
  }

  textPDF(index) {
    const linkSource = this.HOST_URL + this.myAnswer.data[index].answer[0].value;
    const downloadLink = document.createElement('a');
    const fileName = 'sample.pdf';

    downloadLink.href = linkSource;
    downloadLink.download = fileName;
    window.open(downloadLink.href);

  }



  openDialog(): void {
    // tslint:disable-next-line: no-use-before-declare
    const dialogRef = this.dialog.open(EditDialogComponent, {
      width: '500px',
      data: {
        myAnswer: this.myAnswer,
        questionData: this.questionData,
        isRequired: this.isRequired,
        bool: {
          isDate: this.isDate,
          isDatetime: this.isDatetime,
          isString: this.isAnsString,
          isText: this.isAnsText,
          isInt: this.isAnsInt,
          isFloat: this.isAnsFloat,
          isCheck: this.isAnsCheck,
          isImage: this.isAnsImage,
          isFile: this.isAnsFile,
          isDropdown: this.isAnsDropdown,
        },
        checkboxData: this.checkboxData,
        imageShow: this.imageShow,
        fileName: this.fileName,
        selectDropdown: this.selectAnsDropdown,
        fileData: this.fileData,
        fileValue: this.fileValue,
        answerItem: this.answerItem,
        timeOfDatetime: this.timeOfDatetime,
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getAnswer();
      this.getAnswerFormAll();
    });
  }



  getQuestion() {
    this.formService.getFormItem(this.formId).subscribe(data => {
      this.questionData = data;

      this.checkTypeAnswer();
      this.getRequire();
    });
  }

  getRequire() {
    this.isRequired = [];
    const leng = Object.keys(this.questionData.item);
    for (let i = 0; i < leng.length; i++) {
      const require = this.questionData.item[i].required;

      if (require === true) {
        this.isRequired[i] = true;
      }
    }
  }

  checkTypeAnswer() {
    this.selectAnsDropdown = [];
    const leng = Object.keys(this.questionData.item);
    for (let i = 0; i < leng.length; i++) {
      const type = this.questionData.item[i].data_type;

      if (type === 'Date') {
        this.isAnsDate[i] = true;
      } else if (type === 'Datetime') {
        this.isAnsDatetime[i] = true;
      } else if (type === 'String') {
        this.isAnsString[i] = true;
      } else if (type === 'Text') {
        this.isAnsText[i] = true;
      } else if (type === 'Integer') {
        this.isAnsInt[i] = true;
      } else if (type === 'Float') {
        this.isAnsFloat[i] = true;
      } else if (type === 'Checkbox') {
        this.isAnsCheck[i] = true;
      } else if (type === 'Image') {
        this.isAnsImage[i] = true;
      } else if (type === 'File') {
        this.isAnsFile[i] = true;
      } else if (type === 'Dropdown') {
        this.isAnsDropdown[i] = true;
        if (this.myAnswer.data[i].answer) {
          this.selectAnsDropdown[i] = this.myAnswer.data[i].answer[0].value;
        } else {
          this.selectAnsDropdown[i] = 'Select';
        }

      }
    }
    this.answerToQuestion();
  }

  answerToQuestion() {
    this.imageShow = [];
    const leng = Object.keys(this.questionData.item);
    for (let i = 0; i < leng.length; i++) {
      if (this.questionData.item[i].data_type === 'Image') {

        this.questionData.item[i].value = this.myAnswer.data[i].answer[0].values;

        const img: any = [];
        if (this.myAnswer.data[i].answer[0].values != null) {
          this.myAnswer.data[i].answer[0].values.forEach(element => {
            img.push({
              id: element.id,
              image: this.HOST_URL + element.image,
              new_type: false
            });
          });
          this.imageShow[i] = {
            values: img
          };
        } else {
          this.imageShow[i] = {
            values: img
          };
        }

      } else if (this.questionData.item[i].data_type === 'File') {
        if (this.myAnswer.data[i].answer) {
          this.fileValue[i] = {
            name: this.myAnswer.data[i].answer[0].filename,
            value: this.myAnswer.data[i].answer[0].value
          };
          console.log(this.fileValue);
        } else {
          this.fileValue[i] = {
            name: '',
            value: ''
          };
        }

      } else if (this.questionData.item[i].data_type === 'Checkbox') {

        // tslint:disable-next-line:prefer-for-of
        for (let j = 0; j < this.questionData.item[i].dropdown.length; j++) {
          this.questionData.item[i].dropdown[j].boolean = false;
          if (this.myAnswer.data[i].answer) {
            // tslint:disable-next-line:prefer-for-of
            for (let k = 0; k < this.myAnswer.data[i].answer.length; k++) {

              const checkboxValue = this.questionData.item[i].dropdown[j].dropdown_value;
              const answerCheckboxValue = this.myAnswer.data[i].answer[k].value;
              if (checkboxValue.toString() === answerCheckboxValue.toString()) {
                this.questionData.item[i].dropdown[j].boolean = true;
              }
            }
          }
        }
      } else if (this.questionData.item[i].data_type === 'Datetime') {

        if (this.myAnswer.data[i].answer[0].value) {
          // tslint:disable-next-line:max-line-length
          this.questionData.item[i].value = formatDate(new Date(this.myAnswer.data[i].answer[0].value.toString()), 'yyyy-MM-ddThh:mm:ss', 'en-US', '+0700');
          const datetime = this.questionData.item[i].value.split('T');
          this.questionData.item[i].value = datetime[0];
          this.timeOfDatetime[i] = datetime[1];
        }

      } else if (this.questionData.item[i].data_type === 'Dropdown') {

        this.questionData.item[i].dropdown.forEach(element => {
          if (element.dropdown_value.toString() === this.myAnswer.data[i].answer[0].value.toString()) {
            this.questionData.item[i].value = element.id;
          }
        });
      } else {
        if (this.myAnswer.data[i].answer) {
          this.questionData.item[i].value = this.myAnswer.data[i].answer[0].value;

        }
      }

    }

  }

  exportAsXLSX() {
    this.formService.exportXLSX(this.formId).then(data => {

      const fileXLSX: any = data;
      const downloadLink = document.createElement('a');
      downloadLink.href = this.HOST_URL + fileXLSX;
      window.open(downloadLink.href);
    }
    );
  }
}




export interface DialogData {
  myAnswer: any;
  questionData: any;
  isRequired: any[];
  bool: any;
  checkboxData: any;
  isCheckboxBoolean: any[];
  imageShow: any;
  fileName: any;
  selectDropdown: any;
  fileData: any;
  fileValue: any[];
  answerItem: any;
  timeOfDatetime: any;
}


@Component({
  selector: 'app-detailedit-dialog',
  templateUrl: 'detailedit-dialog.html',
})
export class EditDialogComponent implements OnInit {

  answerId = localStorage.getItem('answerId');
  groupId = localStorage.getItem('groupId');
  formId = localStorage.getItem('form_id');

  answer: AnswerModel;
  imgTable = [];
  formData: FormData = new FormData();
  // image
  imageData: any = [];
  selectedFiles: File;
  imageDataDelete: any = [];
  // file
  fileEdit: any = [];
  // datetime
  dateOfDatetime: any = [];
  // save
  isValueEmpty = false;

  constructor(
    private answerService: AnswerService,
    private alerteditansService: AlerteditanswerService,
    public dialogRef: MatDialogRef<EditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private alertRequire: AlertRequireService,
    private formService: FormService
  ) { }

  ngOnInit() {
    this.imageDataDelete = [];
    this.imageData = [];
  }

  getAnswer() {

    this.formService.getAnswerForm(this.formId, this.answerId).subscribe(data => {
      this.data.myAnswer = data;
    });

  }
  onNoClick(): void {
    this.getAnswer();
    this.dialogRef.close();
  }

  handleFileInputExam(event, index) {
    this.imgTable = [];

    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < event.target.files.length; i++) {
      this.selectedFiles = event.target.files[i] as File;
      const reader = new FileReader();
      reader.onload = (evented: any) => {
        const img = evented.target.result;
        this.data.imageShow[index].values.push({
          image: img,
          new_type: true
        });

        this.imgTable.push(evented.target.result);
        const imageText: any = [];
        if (this.imgTable.length > 0) {
          this.imgTable.forEach(element => {
            imageText.push(element.split(',')[1]);
          });
        }
        this.imageData[index] = imageText;

      };
      reader.readAsDataURL(this.selectedFiles);
    }
  }

  handleFile(event, index) {
    const file = event.target.files[0];
    this.data.fileValue[index].name = file.name;
    this.data.fileValue[index].value = file;
    this.fileEdit[index] = file;
  }

  deleteImage(index, indexImg) {

    if (this.data.imageShow[index].values[indexImg].id) {
      const imgshow = this.data.imageShow[index].values[indexImg].id;
      const itemId = this.data.myAnswer.data[index].answer[0].user_item_id;
      this.imageDataDelete.push({
        user_item_id: itemId,
        image_id: imgshow
      });
      this.data.imageShow[index].values.splice(indexImg, 1);

    } else {
      if (this.data.imageShow[index].values[indexImg].new_type === true) {
        this.data.imageShow[index].values.splice(indexImg, 1);
      }
      this.imageData[index] = null;
    }
  }

  dropdownSelect(item, index, indexDropdown) {
    this.data.selectDropdown[index] = item;
    this.data.questionData.item[index].value = this.data.questionData.item[index].dropdown[indexDropdown].id;
  }

  checkboxSelect(index, indexCheckbox) {

    // tslint:disable-next-line:max-line-length
    this.data.questionData.item[index].dropdown[indexCheckbox].boolean = !this.data.questionData.item[index].dropdown[indexCheckbox].boolean;

  }

  EndDateChange($event, index) {
    this.dateOfDatetime[index] = formatDate(new Date($event.value.toString()), 'yyyy-MM-ddT', 'en-US', '+0700');
  }
  closeTime(index) {
  }

  editAnswer() {

    this.data.answerItem = [];
    this.formData.delete('user_form_item[][order_no]');
    this.formData.delete('user_form_item[][value]');
    this.formData.delete('user_form_item[][boolean]');
    let index = 0;
    let isRequire = false;

    this.data.questionData.item.forEach(element => {
      if (element.required) {
        if (element.data_type === 'Checkbox') {
          const countCheckbox = element.dropdown.length;
          let numberCount = 0;
          element.dropdown.forEach(drop => {
            if (!drop.boolean) {
              numberCount++;
            }
            this.data.answerItem.push({
              order_no: (element.order_no).toString(),
              value: drop.id.toString(),
              boolean: drop.boolean.toString()
            });
          });
          if (numberCount === countCheckbox) {
            isRequire = true;
          }
        } else if (element.data_type === 'Date') {
          if (element.value) {
            this.data.answerItem.push({
              order_no: (element.order_no).toString(),
              value: formatDate(new Date(element.value.toString()), 'yyyy-MM-dd', 'en-US', '+0700'),
              boolean: false
            });
          } else {
            isRequire = true;
          }

        } else if (element.data_type === 'Datetime') {
          if (this.dateOfDatetime[index]) {
            this.data.answerItem.push({
              order_no: (element.order_no).toString(),
              value: this.dateOfDatetime[index] + this.data.timeOfDatetime[index],
              boolean: false
            });
          } else {
            this.data.answerItem.push({
              order_no: (element.order_no).toString(),
              value: element.value + 'T' + this.data.timeOfDatetime[index],
              boolean: false
            });
          }

        } else if (element.data_type === 'Image') {
          this.formData.append('user_form_item[][order_no]', element.order_no.toString());

          if (this.data.imageShow[index].values.length > 0) {
            const imageText = [];
            this.data.imageShow[index].values.forEach(img => {
              if (img.new_type) {
                imageText.push(img.image.split(',')[1]);
              }
            });

            if (imageText.length > 0) {
              imageText.forEach(img => {
                this.formData.append('user_form_item[][value][]', img);
              });
            }

          } else {
            isRequire = true;
          }

        } else if (element.data_type === 'File') {
          if (this.fileEdit[index]) {
            this.formData.append('user_form_item[][order_no]', element.order_no.toString());
            this.formData.append('user_form_item[][value]', this.fileEdit[index]);

          } else {
            if (this.data.fileValue[index].value !== null) {
              this.formData.append('user_form_item[][order_no]', element.order_no.toString());
            } else {
              isRequire = true;
            }
          }

        } else {
          if (!element.value) {
            isRequire = true;
          } else {
            this.data.answerItem.push({
              order_no: (element.order_no).toString(),
              value: element.value.toString(),
              boolean: false
            });
          }

        }
      } else {
        if (element.data_type === 'Checkbox') {
          element.dropdown.forEach(drop => {
            this.data.answerItem.push({
              order_no: (element.order_no).toString(),
              value: drop.id.toString(),
              boolean: drop.boolean.toString()
            });
          });

        } else if (element.data_type === 'Image') {
          this.formData.append('user_form_item[][order_no]', element.order_no.toString());
          if (this.data.imageShow[index].values.length > 0) {
            const imageText = [];
            this.data.imageShow[index].values.forEach(img => {
              if (img.new_type) {
                imageText.push(img.image.split(',')[1]);
              }
            });

            if (imageText.length > 0) {
              imageText.forEach(img => {
                this.formData.append('user_form_item[][value][]', img);
              });
            }
          }

        } else if (element.data_type === 'File') {
          if (this.fileEdit[index]) {
            this.formData.append('user_form_item[][order_no]', element.order_no.toString());
            this.formData.append('user_form_item[][value]', this.fileEdit[index]);
          }

        } else {
          this.formData.append('user_form_item[][order_no]', element.order_no.toString());
          this.formData.append('user_form_item[][value]', null);

        }
      }
      index++;
    });

    this.answer = {
      user_form_item: this.data.answerItem
    };
    this.answer.user_form_item.forEach(element => {
      this.formData.append('user_form_item[][order_no]', element.order_no.toString());
      this.formData.append('user_form_item[][value]', element.value);
      this.formData.append('user_form_item[][boolean]', element.boolean.toString());
    });

    if (!isRequire) {

      this.alerteditansService.alertEditQuestion(this.formData, this.answerId, this.groupId, this.formId).then(resolve => {
        if (resolve === true) {
          if (this.imageDataDelete.length > 0) {
            this.answerService.deleteImage(this.imageDataDelete).then(data => {
              if (data) {
                this.onNoClick();
                this.dialogRef.close();
              }

            });
          } else {
            this.onNoClick();
          }

        }
      });
    } else {
      this.alertRequire.alertRequire();
    }
    isRequire = false;
  }
}


