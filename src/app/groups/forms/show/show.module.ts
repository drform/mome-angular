import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ShowRoutingModule } from './show-routing.module';
import { ShowComponent, EditDialogComponent } from '../show/show.component';
import { ShowinfoComponent } from './showinfo/showinfo.component';

import { AngularFileUploaderModule } from 'angular-file-uploader';

import { DemoMaterialModule } from '../../../material-module';

import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { NgxTypeaheadModule } from 'ngx-typeahead';

import { NumberDirective } from './answer/numbers-only.directive';

import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { LOCALE_ID } from '@angular/core';
import localeTh from '@angular/common/locales/th';
import { registerLocaleData } from '@angular/common';

registerLocaleData(localeTh, 'th');

@NgModule({
  entryComponents: [EditDialogComponent],
  declarations: [
    ShowComponent,
    EditDialogComponent,
    ShowinfoComponent,
    NumberDirective
  ],
  imports: [
    CommonModule,
    ShowRoutingModule,
    FormsModule,
    AngularFileUploaderModule,
    DemoMaterialModule,
    NgxTypeaheadModule,
    NgxMaterialTimepickerModule,
    TypeaheadModule.forRoot()
  ],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: 'th'
    }
  ],
  bootstrap: [ShowComponent]
})
export class ShowModule { }
