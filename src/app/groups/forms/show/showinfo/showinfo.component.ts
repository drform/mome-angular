import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Host } from '../../../../../environments/environment';
import { FormService } from '../../../../services/form.service';
import { DateAdapter } from '@angular/material';
@Component({
  selector: 'app-showinfo',
  templateUrl: './showinfo.component.html',
  styleUrls: ['./showinfo.component.css']
})
export class ShowinfoComponent implements OnInit {

  HOST_URL = Host.url;
  formId = localStorage.getItem('form_id');
  formName = localStorage.getItem('formName');
  groupId = localStorage.getItem('groupId');
  groupName = localStorage.getItem('groupName');


  dataImageValue: any;
  dataFileValue: any;
  imageData = [];
  fileData = [];

  imagenull = false;
  filenull = false;

  searchPreImage = '';
  searchAfImage = '';
  searchPreDate = '';
  searchAfDate = '';

  showImage;

  constructor(
    private formService: FormService,
    private route: ActivatedRoute,
    private dateAdapter: DateAdapter<Date>,
    private client: HttpClient,
  ) { }

  ngOnInit() {
    this.dateAdapter.setLocale('th-TH');
    this.getInfoImage();
    this.getInfoFile();
  }

  getInfoImage() {
    this.imageData = [];
    let param: any;
    if (this.searchPreImage && this.searchAfImage) {
      param = {
        type: 'image',
        search: this.searchPreImage + ',' + this.searchAfImage
      };
    } else if (this.searchPreImage) {
      param = {
        type: 'image',
        search: this.searchPreImage
      };
    } else {
      param = {
        type: 'image',
      };
    }

    this.formService.getAnswerFormAll(this.formId, param).subscribe(data => {
      this.dataImageValue = data;      
      if (!this.dataImageValue) {
        this.imagenull = true;
      } else {
        this.imagenull = false;
      }

    });
  }

  getInfoFile() {
    this.fileData = [];
    let param: any;
    if (this.searchPreDate && this.searchAfDate) {
      param = {
        type: 'file',
        search: this.searchPreDate + ',' + this.searchAfDate
      };
    } else if (this.searchPreDate) {
      param = {
        type: 'file',
        search: this.searchPreDate
      };
    } else {
      param = {
        type: 'file',
      };
    }

    this.formService.getAnswerFormAll(this.formId, param).subscribe(data => {
      this.dataFileValue = data;
      if (!this.dataFileValue) {
        this.filenull = true;
      } else {
        this.filenull = false;
      }

    });
  }

  fileDownload(index) {
    const linkSource = this.HOST_URL + this.dataFileValue[0].data[index].file;
    const downloadLink = document.createElement('a');
    const fileName = 'sample.pdf';

    downloadLink.href = linkSource;
    downloadLink.download = fileName;
    window.open(downloadLink.href);
  }

  selectImage(value) {
    this.showImage = value;
  }
  
  DownloadAll(day) {
    this.dataImageValue[day].data.forEach(img => {
      this.client.get(this.HOST_URL + img.image, {responseType: 'arraybuffer'}).subscribe((res) => {
        const a = document.createElement('a');
        const file = new Blob([res], {type: 'image/jpeg'});
        a.href = URL.createObjectURL(file);
        a.download = ''+Date.now();
        a.click();
      });      
    });    
  }
}
