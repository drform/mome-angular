import { Component, OnInit } from '@angular/core';
import { Host } from '../../../environments/environment';
import { GroupModel } from '../../models/group.model';
import { GroupService } from '../../services/group.service';
import { AlertgroupService } from '../../shares/alertgroup.service';
import { AlertImagesizeService } from '../../shares/alert-imagesize.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
@Component({
  selector: 'app-groupedit',
  templateUrl: './groupedit.component.html',
  styleUrls: ['./groupedit.component.css']
})
export class GroupeditComponent implements OnInit {
  HOST_URL = Host.url;
  groupData: GroupModel;
  groupSelect: any;
  groupId: any;

  imageData: string;
  imageShow: any;
  imageName = 'Choose Image';
  selectedFiles: File;

  limit = 5;
  offset = 0;
  shareRoleId = 1;
  order = 'id ASC';
  keyword = '';
  saveData: any = [];

  isBackForm = false;
  isNextForm = false;
  stackPage = 0;

  constructor(
    private alertgroupService: AlertgroupService,
    private groupService: GroupService,
    private alertImage: AlertImagesizeService
  ) { }

  ngOnInit() {
    this.getGroup();
  }
  backPage() {
    this.stackPage -= 1;
    this.offset -= 5;
    this.getGroup();
  }
  nextPage() {
    this.isBackForm = true;
    this.stackPage += 1;
    this.offset += 5;
    this.getGroup();
  }
  getGroup() {
    const param = {
      limit: this.limit,
      offset: this.offset,
      share_role_id: this.shareRoleId,
      order: this.order,
      keyword: this.keyword
    };
    this.groupService.getGroup(param).subscribe(data => {
      if (Object.keys(data).length !== 0) {
        this.groupData = data;
        if (this.groupData.amount >= 5) {
          this.isNextForm = true;
        } else {
          this.isNextForm = false;
        }

        if (this.stackPage === 0) {
          this.isBackForm = false;
        } else {
          this.isBackForm = true;
        }
      }
    });
  }

  getDetail(index) {
    this.groupService.getSelectGroup(this.groupData.groups[index].id).subscribe(data => {
      this.groupSelect = data;
      this.imageShow = this.HOST_URL + this.groupSelect.image;
      this.groupId = this.groupSelect.id;
    });
  }

  handleImageInput(event) {
    this.selectedFiles = event.target.files[0] as File;
    if (this.selectedFiles.size < 10 * 1024 * 1024) {
      this.imageName = this.selectedFiles.name;
      const reader = new FileReader();
      reader.onload = (evented: any) => {
        let img = evented.target.result;
        this.imageShow = evented.target.result;
        img = img.split(',')[1];
        this.imageData = img;
      };
      reader.readAsDataURL(this.selectedFiles);
    } else {
      this.alertImage.alertImageOverSize();
    }

  }
  editGroup() {
    let groupDataInput: any;
    if (this.imageData) {
      groupDataInput = {
        group: {
          title: this.groupSelect.title,
          description: this.groupSelect.description,
          image: this.imageData
        }
      };
    } else {
      groupDataInput = {
        group: {
          title: this.groupSelect.title,
          description: this.groupSelect.description,
          image: ''
        }
      };
    }
    if (this.groupSelect.title !== '') {
      this.alertgroupService.alertEditGroup(groupDataInput, this.groupId);
      this.saveData = [];
      Swal.fire({
        title: 'Confirm Edit Group?',
        text: 'Confirm edit this group.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3885DE',
        cancelButtonColor: '#e41c44',
        confirmButtonText: 'Yes',
      }).then((result) => {
        if (result.value) {
          this.groupService.editGroup(groupDataInput, this.groupId).subscribe(data => {
            this.saveData = data;
            if (this.saveData.message === 'Update group successfully.') {
              Swal.fire({
                title: 'Edit group Successed!',
                text: 'Your edit group successed.',
                type: 'success',
              });
              this.saveData = null;
              this.groupSelect = null;
              this.getGroup();
            } else {
              Swal.fire({
                title: 'Something wrong!',
                text: 'Can not edit this group',
                type: 'error',
                confirmButtonColor: '#3885DE',
                confirmButtonText: 'OK'
              });
              this.getGroup();
            }
          });
        }
      });
    } else {
      this.alertgroupService.alertGroupEmpty();
    }
  }
  deleteGroup() {
    this.saveData = [];
    Swal.fire({
      title: 'Confirm Delete Group?',
      text: 'Confirm delete this group.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3885DE',
      cancelButtonColor: '#e41c44',
      confirmButtonText: 'Yes',
    }).then((result) => {
      if (result.value) {
        this.groupService.deleteGroup(this.groupId).subscribe(data => {
          this.saveData = data;
          if (this.saveData.message === 'Delete Successfully') {
            Swal.fire({
              title: 'Delete group Successed!',
              text: 'Your delete group successed.',
              type: 'success',
            });
            this.saveData = null;
            this.groupSelect = null;
            this.getGroup();
          } else {
            Swal.fire({
              title: 'Something wrong!',
              text: 'Can not delete this group',
              type: 'error',
              confirmButtonColor: '#3885DE',
              confirmButtonText: 'OK'
            });
          }

        });
      }
    });
  }
}
