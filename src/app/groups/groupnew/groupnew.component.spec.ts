import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupnewComponent } from './groupnew.component';

describe('GroupnewComponent', () => {
  let component: GroupnewComponent;
  let fixture: ComponentFixture<GroupnewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupnewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupnewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
