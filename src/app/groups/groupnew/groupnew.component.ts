import { Component, OnInit } from '@angular/core';
import { AlertgroupService } from '../../shares/alertgroup.service';
import { AlertImagesizeService } from '../../shares/alert-imagesize.service';

@Component({
  selector: 'app-groupnew',
  templateUrl: './groupnew.component.html',
  styleUrls: ['./groupnew.component.css']
})
export class GroupnewComponent implements OnInit {

  title: any = '';
  description: any = '';

  imageData: string;
  imageShow = 'assets/img/default_pic.png';
  imageName = 'Choose Image';
  selectedFiles: File;
  constructor(
    private alertgroupService: AlertgroupService,
    private alertImage: AlertImagesizeService
  ) { }

  ngOnInit() {
  }

  handleImageInput(event) {
    this.selectedFiles = event.target.files[0] as File;
    if (this.selectedFiles.size < 10 * 1024 * 1024) {
      this.imageName = this.selectedFiles.name;
      const reader = new FileReader();
      reader.onload = (evented: any) => {
        let img = evented.target.result;
        this.imageShow = evented.target.result;
        img = img.split(',')[1];
        this.imageData = img;
      };
      reader.readAsDataURL(this.selectedFiles);
    } else {
      this.alertImage.alertImageOverSize();
      this.imageName = 'Choose image';
    }

  }

  createGroup() {
    const groupData: any = {
      group: {
        title: this.title,
        description: this.description,
        image: this.imageData
      }
    };
    if (this.title !== '') {
      this.alertgroupService.alertCreateGroup(groupData);
    } else {
      this.alertgroupService.alertGroupEmpty();
    }


  }

}
