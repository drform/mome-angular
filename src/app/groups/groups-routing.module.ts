import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GroupsComponent } from '../groups/groups.component';
import { GroupshowComponent } from './groupshow/groupshow.component';
import { GroupnewComponent } from './groupnew/groupnew.component';
import { GroupeditComponent } from './groupedit/groupedit.component';

const routes: Routes = [
  {
    path: '',
    component: GroupsComponent,
    children: [
      { path: '', component: GroupshowComponent },
      { path: 'new', component: GroupnewComponent },
      { path: 'edit', component: GroupeditComponent },
      {
        path: ':id',
        loadChildren: '../groups/forms/forms.module#FormsModules',
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GroupsRoutingModule { }
