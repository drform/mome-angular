import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '../Layout/layout.module';
import { FormsModule } from '@angular/forms';

import { GroupsRoutingModule } from './groups-routing.module';
import { GroupsComponent } from '../groups/groups.component';
import { GroupshowComponent } from './groupshow/groupshow.component';
import { GroupnewComponent } from './groupnew/groupnew.component';
import { GroupeditComponent } from './groupedit/groupedit.component';
import { DemoMaterialModule } from '../material-module';

@NgModule({
  imports: [
    CommonModule,
    GroupsRoutingModule,
    FormsModule,
    LayoutModule,
    DemoMaterialModule
  ],
  declarations: [
    GroupsComponent,
    GroupshowComponent,
    GroupnewComponent,
    GroupeditComponent
  ],
  exports: [GroupeditComponent]

})
export class GroupsModule { }
