import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Host } from '../../../environments/environment';
import { GroupModel } from '../../models/group.model';
import { GroupService } from '../../services/group.service';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user.model';
@Component({
  selector: 'app-groupshow',
  templateUrl: './groupshow.component.html',
  styleUrls: ['./groupshow.component.css']
})
export class GroupshowComponent implements OnInit {
  HOST_URL = Host.url;
  user: User;
  isAdmin: boolean;
  groupData: GroupModel;

  limit = 12;
  offset = 0;
  shareRoleId = 1;
  order = 'id ASC';
  keywords = '';

  isLoadForm = false;
  isEmptyGroup = false;
  constructor(
    private route: Router,
    private groupService: GroupService,
    private userservice: UserService,
  ) { }

  ngOnInit() {
    this.getGroups();
    this.getUser();
  }

  getGroups() {
    this.isEmptyGroup = false;
    this.isLoadForm = false;
    const param = {
      limit: this.limit,
      offset: this.offset,
      share_role_id: this.shareRoleId,
      order: this.order,
      keyword: this.keywords
    };
    this.groupService.getGroup(param).subscribe(data => {
      this.groupData = data;
      if (Object.keys(this.groupData).length === 0) {
        this.isEmptyGroup = true;
      }
      if (this.groupData.amount >= 12) {
        this.isLoadForm = true;
      }
    });
  }

  getUser() {
    this.userservice.getUser().subscribe(state => {
      this.user = state;
      if (this.user != null) {
        if (this.user.user_info.role === 'User Admin' || this.user.user_info.role === 'Admin') {
          this.isAdmin = true;
        } else {
          this.isAdmin = false;
        }
      }
    });
  }

  selectGroup(index) {
    const groupname = 'Group: ' + this.groupData.groups[index].title;
    localStorage.setItem('groupName', groupname);
    this.route.navigate(['/groups/' + this.groupData.groups[index].id]);
  }

  loadForm() {
    this.offset += 12;
    this.getGroups();
  }
}
