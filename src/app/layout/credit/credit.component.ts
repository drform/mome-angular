import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-credit',
  templateUrl: './credit.component.html',
  styleUrls: ['./credit.component.css']
})
export class CreditComponent implements OnInit {

  userData = [
    {
      name: 'Jirayu Pharang',
      image: 'assets/team/art.jpg',
      role: 'Android Developer',
      university: 'Khon Kaen University',
      tel: '082-8536232',
      email: 'p_jirayu@kkumail.com'
    },
    {
      name: 'Jirathiwat Charoenwongsakul',
      image: 'assets/team/mook.jpg',
      role: 'Back-End Developer',
      university: 'Khon Kaen University',
      tel: '095-8815968',
      email: 'jirathivat.s@kkumail.com'
    },
    {
      name: 'Asanee Sarathanong',
      image: 'assets/team/earth.jpg',
      role: 'Web Front-Back Developer',
      university: 'Mahasarakham University',
      tel: '080-7505057',
      email: 'EnvyHotMN@hotmail.co.th'
    },
    {
      name: 'Rutchanai Chaiyasit',
      image: 'assets/team/mocca.jpg',
      role: 'Web Developer',
      university: 'Mahasarakham University',
      tel: '093-4920570',
      email: 'rutchanai.chai@gmail.com'
    },
    {
      name: 'Rutchanon Putpa',
      image: 'assets/team/rutcha.jpg',
      role: 'iOS Developer',
      university: 'Mahasarakham University',
      tel: '095-6608908',
      email: 'therw101@hotmail.com'
    },
  ];

  constructor() { }

  ngOnInit() {
  }

}
