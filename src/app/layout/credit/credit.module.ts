import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreditRoutingModule } from './credit-routing.module';
import { CreditComponent } from './credit.component';
import { FormsModule } from '@angular/forms';
import { DemoMaterialModule } from '../../material-module';

@NgModule({
  declarations: [
    CreditComponent
  ],
  imports: [
    CommonModule,
    CreditRoutingModule,
    FormsModule,
    DemoMaterialModule
  ]
})
export class CreditModule { }
