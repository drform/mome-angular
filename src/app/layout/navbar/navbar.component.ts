import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from '../../services/user.service';
import { Host } from '../../../environments/environment';
import { User } from '../../models/user.model';
import { GroupModel } from '../../models/group.model';
import { GroupService } from '../../services/group.service';
import { AlertLogoutService } from '../../shares/alert-logout.service';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  HOST_URL = Host.url;
  user: User;
  isAdmin: boolean;
  groupData: GroupModel;
  limit = 12;
  offset = 0;
  shareRoleId = 1;
  order = 'id ASC';
  keywords = '';
  keySearch = '';
  constructor(
    private routes: Router,
    private userservice: UserService,
    private groupService: GroupService,
    private alertLogoutService: AlertLogoutService
  ) { }

  ngOnInit() {
    this.getUser();
    this.getGroups();
  }

  getUser() {
    this.userservice.getUser().subscribe(state => {
      this.user = state;
      if (this.user != null) {
        if (this.user.user_info.role === 'User Admin' || this.user.user_info.role === 'Admin') { // Admin
          this.isAdmin = true;
        } else {
          this.isAdmin = false;
        }
      }
    });
  }

  getGroups() {
    const param = {
      limit: this.limit,
      offset: this.offset,
      share_role_id: this.shareRoleId,
      order: this.order,
      title: this.keywords
    };
    this.groupService.getGroup(param).subscribe(data => {
      this.groupData = data;
    });

  }

  selectGroupMyForm(groupId, index) {
    localStorage.setItem('groupName', this.groupData[index].title);
    this.routes.navigate(['groups/' + groupId]);
    this.refreshRoute();
  }

  selectGroupShareForm(groupId, index) {
    localStorage.setItem('groupId', groupId);
    this.routes.navigate(['groups/' + groupId + '/share']);
    this.refreshRoute();
  }

  selectGroupNew(groupId) {
    this.routes.navigate(['groups/' + groupId + '/new']);
    this.refreshRoute();
  }

  refreshRoute() {
    // tslint:disable-next-line:only-arrow-functions
    this.routes.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
  }


  signOut() {
    this.alertLogoutService.alertLogout().then(data => {
      if (data) {
      }
    });
  }
}
