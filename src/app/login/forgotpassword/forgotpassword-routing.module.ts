import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ForgotpasswordComponent } from '../forgotpassword/forgotpassword.component';

const routes: Routes = [
  { path: '', component: ForgotpasswordComponent },
  {
    path: 'resetpassword',
    loadChildren: '../forgotpassword/resetpassword/resetpassword.module#ResetpasswordModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ForgotpasswordRoutingModule { }
