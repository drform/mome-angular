import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { MessageModel } from '../../models/message.model';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ForgotpasswordComponent implements OnInit {

  saveData: MessageModel;
  pin;
  alertText: any;
  isLoadingPin = false;
  isSubmitClicked = false;
  constructor(
    private routes: Router,
    private authService: AuthService
  ) { }

  ngOnInit() {
    localStorage.setItem('location', this.routes.url);
  }

  checkPin() {
    this.isLoadingPin = true;
    this.isSubmitClicked = true;
    this.authService.forgotToken(this.pin).then(data => {
      this.saveData = data;
      if (this.saveData.error) {
        this.alertText = this.saveData.error;
      } else {
        localStorage.setItem('pin', this.pin);
        this.routes.navigate(['/forgotpassword/resetpassword']);
      }
      this.isLoadingPin = false;
      this.isSubmitClicked = false;
    });
  }

}
