import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ForgotpasswordRoutingModule } from './forgotpassword-routing.module';
import { ForgotpasswordComponent } from '../forgotpassword/forgotpassword.component';
import { DemoMaterialModule } from '../../material-module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ForgotpasswordComponent,
  ],
  imports: [
    CommonModule,
    ForgotpasswordRoutingModule,
    DemoMaterialModule,
    FormsModule
  ]
})
export class ForgotpasswordModule { }
