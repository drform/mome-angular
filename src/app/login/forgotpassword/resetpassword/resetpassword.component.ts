import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../../services/auth.service';
import { MessageModel } from '../../../models/message.model';
import { AlertResetpasswordService } from '../../../shares/alert-resetpassword.service';
@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css']
})
export class ResetpasswordComponent implements OnInit {

  newpassword = '';
  confirmpassword = '';
  isNewPassHide = true;
  isConfirmPassHide = true;
  saveData: MessageModel;
  pin = localStorage.getItem('pin');

  isSubmitClicked = false;
  isLoadingPin = false;
  alertText;

  constructor(
    private routes: Router,
    private authService: AuthService,
    private alertResetpass: AlertResetpasswordService
  ) { }

  ngOnInit() {
    localStorage.setItem('location', this.routes.url);
  }

  checkPassword() {
    this.isSubmitClicked = true;
    this.isLoadingPin = true;
    if (this.newpassword.length < 8 || this.confirmpassword.length < 8) {
      this.alertText = 'Password is lower 8 charater.';
      this.isSubmitClicked = false;
      this.isLoadingPin = false;

    } else if (this.newpassword === '' || this.confirmpassword === '') {
      this.alertText = 'Password is empty';
      this.isSubmitClicked = false;
      this.isLoadingPin = false;

    } else if (this.newpassword === this.confirmpassword) {
      this.authService.forgotPassword(this.pin, this.newpassword).then(data => {
        this.saveData = data;
        if (this.saveData.error) {
          this.alertText = this.saveData.error;
        } else {
          this.alertResetpass.passwordReset().then(data => {
            this.routes.navigate(['/signin']);
          });
        }
        this.isSubmitClicked = false;
        this.isLoadingPin = false;

      });
    } else {
      this.alertText = 'Password is not Match.';
      this.isSubmitClicked = false;
      this.isLoadingPin = false;
    }
  }

}
