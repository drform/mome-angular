import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResetpasswordRoutingModule } from './resetpassword-routing.module';
import { ResetpasswordComponent } from '../resetpassword/resetpassword.component';
import { DemoMaterialModule } from '../../../material-module';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ResetpasswordRoutingModule,
    FormsModule,
    DemoMaterialModule
  ],
  declarations: [
    ResetpasswordComponent
  ],
})
export class ResetpasswordModule { }
