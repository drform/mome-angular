import { Component, OnInit, Inject } from '@angular/core';
import { LoginModel } from '../../models/login.model';
import { User } from '../../models/user.model';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';

import { Store } from '@ngrx/store';
import { AppState } from '../../app.state';
import * as UserActions from '../../actions/user.actions';

import { AlertService } from '../../shares/alert.service';
import { MessageModel } from '../../models/message.model';
import { Location } from '@angular/common';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  login: LoginModel = {
    username: '',
    password: ''
  };
  user: User;
  isLoading = false;
  tokenData;
  pass1Data;
  pass2Data;
  saveData: MessageModel;

  // toggle type password
  isPassHide = true;

  email;
  isLoadingEmail = false;

  constructor(
    private routes: Router,
    private authService: AuthService,
    private store: Store<AppState>,
    private alertService: AlertService,
    private location: Location,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.store.dispatch(new UserActions.RemoveUser());
    localStorage.removeItem('location');
  }

  signIn() {
    this.isLoading = true;
    if (this.login.username && this.login.password) {
      this.authService.postSignin(this.login).subscribe(data => {
        this.user = data;
        if (this.user) {
          if (!this.user.error) {
            this.store.dispatch(new UserActions.AddUser(this.user));
            this.routes.navigate(['/groups']);
          } else {
            this.login.username = '';
            this.login.password = '';
          }
          this.isLoading = false;
        }

      }, (error) => {
        this.isLoading = false;
        this.alertService.alertLogin();
      });
    } else {
      this.isLoading = false;
      this.alertService.alertLoginNull();
    }

  }

  openDialog(): void {
    const dialogRef = this.dialog.open(SendEmailDialog, {
      width: '400px',
    });
  }
}

export interface DialogData {
  email: string;
}

@Component({
  selector: 'app-sendemail-dialog',
  templateUrl: 'sendemail-dialog.html',
})
export class SendEmailDialog {

  isLoadingEmail = false;
  isSubmitClicked = false;
  saveData: MessageModel;
  email;
  textAlert: any;

  constructor(
    public dialogRef: MatDialogRef<SendEmailDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private authService: AuthService,
    private routes: Router,
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  checkEmail() {
    this.isLoadingEmail = true;
    this.isSubmitClicked = true;
    this.authService.forgotEmail(this.email).then(data => {
      this.saveData = data;
      if (this.saveData.error) {
        console.log(this.saveData.error);
        this.textAlert = this.saveData.error;
      } else {
        this.onNoClick();
        this.routes.navigate(['/forgotpassword']);
      }
      this.isLoadingEmail = false;
      this.isSubmitClicked = false;

    });
  }

}

