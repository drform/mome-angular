import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SigninRoutingModule } from './signin-routing.module';
import { SigninComponent, SendEmailDialog } from '../signin/signin.component';
import { DemoMaterialModule } from '../../material-module';
import { FormsModule } from '@angular/forms';

@NgModule({
  entryComponents: [SendEmailDialog],
  declarations: [
    SigninComponent,
    SendEmailDialog
  ],
  imports: [
    CommonModule,
    SigninRoutingModule,
    FormsModule,
    DemoMaterialModule,
  ]
})
export class SigninModule { }
