import { Component, OnInit, HostListener } from '@angular/core';
import { RegisterModel } from '../../models/register.model';
import { AuthService } from '../../services/auth.service';
import { AlertUserService } from '../../shares/alert-user.service';
import { AlertImagesizeService } from '../../shares/alert-imagesize.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  register: RegisterModel;
  confirmpassword: string;

  username: any = '';
  password: any = '';
  confirmPassword: any = '';
  phone: any = '';
  email: any = '';

  imageUrl = '/assets/img/default_pic.png';
  imageShow = '/assets/img/default_pic.png';
  imageName = 'Choose image';
  selectedFiles: File;

  constructor(
    private authService: AuthService,
    private alertUserService: AlertUserService,
    private alertImage: AlertImagesizeService,
    private routes: Router,
  ) { }

  ngOnInit() {
    localStorage.setItem('location', this.routes.url);
  }

  handleFileInput(event) {
    this.selectedFiles = event.target.files[0] as File;
    if (this.selectedFiles) {
      if (this.selectedFiles.size < 10 * 1024 * 1024) {
        const reader = new FileReader();
        reader.onload = (evented: any) => {
          this.imageShow = evented.target.result;
          this.imageName = this.selectedFiles.name;
          this.imageUrl = this.imageShow.split(',')[1];
        };
        reader.readAsDataURL(this.selectedFiles);
      } else {
        this.alertImage.alertImageOverSize();
        this.imageName = 'Choose image';
        this.imageShow = '/assets/img/default_pic.png';
      }
    }
  }

  signUp() {
    if (this.password === this.confirmPassword) {
      const userData = {
        user: {
          username: this.username,
          password: this.password,
          name: 'Insert your name',
          email: this.email,
          tel: this.phone,
          position: 'Insert your position',
          image: this.imageUrl
        }
      };

      this.alertUserService.alertRegister(userData);
    }
  }
  @HostListener('window:keyup', ['$event'])
  onKeyUp($event: KeyboardEvent) {
    if (!($event.keyCode >= 48 && $event.keyCode <= 57)) {
      this.phone = '';
    }
  }

}
