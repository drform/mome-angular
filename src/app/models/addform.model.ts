export interface FormAdd {
  form?: {
    title?: string;
    description?: string;
    group_id?: string,
    image?: string;
  };
  form_item?: any;
}

export interface FormItem {
  id?: number;
  order_no: number;
  data_type: string;
  title: string;
  description: string;
  required: boolean;
  av_search: boolean;
  value: Dropdown[];
  is_dropdown?: boolean;
  is_checkbox?: boolean;
  is_newtype?: boolean;
  is_datatype?: string;
}

export interface Dropdown {
  data: string;
  is_removable?: boolean;
}

