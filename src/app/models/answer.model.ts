export interface AnswerModel {
  user_form_item: AnswerItem[];
}

export interface AnswerItem {
  order_no: number;
  value: string;
  boolean?: boolean;
}

export interface AnswerClicked {
  data?: ClickedDetail[];
}
export interface ClickedDetail {
  id: number;
  order_no: number;
  data_type_id: number;
  question: string;
  answer?: ArrayDetail[];
}
export interface ArrayDetail {
  user_item_id?: number;
  choice_id?: number;
  value?: string;
  values?: Album[];
  filename?: string;
}
export interface Album {
  user_item_id?: number;
  id?: number;
  image?: string;
}

