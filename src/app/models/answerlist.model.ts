export interface AnswerList {
  user_form?: UserFormItem[];
  form?: FormDetail;
  amount?: number;
}
export interface UserFormItem {
  id?: number;
  user_id?: number;
  form_id?: number;
  created_at?: string;
  updated_at?: string;
  name?: string;
  image?: string;
}
export interface FormDetail {
  id?: number;
  title?: string;
  description?: string;
  image?: string;
}
