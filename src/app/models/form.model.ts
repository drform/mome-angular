export interface FormModel {
  id?: number;
  title?: string;
  description?: string;
  owner?: string;
  share_role?: string;
  image?: string;
  error?: string;
}

export interface FormList {
  form?: Form;
  answer?: Answer[];
  error?: string;
}

export interface Form {
  id: number;
  user_id: number;
  title: string;
  description: string;
  name: string[];
  question: Question[];
  share: Share[];
  image: string;
}

export interface Dropdown {
  id: number;
  form_item_id: number;
  dropdown_value: string;
}

export interface Question {
  id: number;
  form_id: number;
  order_no: number;
  data_type_id: number;
  title: string;
  description: string;
  required: boolean;
  av_search: boolean;
  dropdown: Dropdown[];
  data_type: string;
  value: string;
}

export interface Share {
  id: number;
  user_id: number;
  form_id: number;
  share_role: string;
}

export interface Answer {
  id: number;
  form_id: number;
  name: string;
  image?: string;
}

export interface ReturnData {
  data?: any;
  message?: any;
  error?: ReturnError;
}

export interface ReturnError {
  title?: string;
  description?: string;
}


















