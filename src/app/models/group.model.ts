export interface Group {
  id?: number;
  user_id?: number;
  title?: string;
  description?: string;
  created_at?: Date;
  updated_at?: Date;
  image?: any;
}
export interface GroupModel {
  amount?: number;
  groups?: Group[];
}
