export interface ImageModel {
  url: string;
  type: string;
}
