
export interface MessageModel {
  error?: Error;
  message?: string;
  data?: Data;
}
export interface Error {
  title?: any;
  description?: any;
}
export interface Data {
  description?: string;
  group_id?: number;
  id?: number;
  image?: string;
  name?: string;
  question?: any;
  share?: any;
  title?: string;
  user_id?: number;
}
