export interface FormQuestion {
  item?: Array<Question>;
}

export interface Question {
  id?: number;
  form_id?: number;
  order_no?: number;
  data_type_id?: number;
  title?: string;
  description?: string;
  required?: boolean;
  av_search?: boolean;
  dropdown?: Dropdown[];
  data_type?: string;
  value?: any;
  imageUrl?: any;
  is_dropdown?: boolean;
  is_checkbox?: boolean;
}

export interface Checkbox {
  order_no?: number;
  value?: any;
  dropdown_value?: any;
  boolean?: boolean;
}

export interface Dropdown {
  id: number;
  form_item_id: number;
  dropdown_value: string;
  boolean?: boolean;
}


