export interface RegisterModel {
  username?: string;
  password?: string;
  name?: string;
  email?: string;
  tel?: number;
  position?: string;
  image?: [];
}
