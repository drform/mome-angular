export interface FormQuestionSearch {
  item?: Question[];
}

export interface Question {
  id?: number;
  form_id?: number;
  order_no?: number;
  data_type_id?: number;
  title?: string;
  description?: string;
  required?: boolean;
  av_search?: boolean;
  dropdown?: Dropdown[];
  data_type?: string;
  value?: string;
}
export interface Dropdown {
  id?: number;
  form_item_id?: number;
  dropdown_value?: string;
}

export interface AnswerList {
  answer?: Answer[];
  user_form?: Answer[];
}
export interface Answer {
  id?: number;
  user_id?: number;
  form_id?: number;
  created_at?: Date;
  updated_at?: Date;
  name?: string;
  image?: string;
}
