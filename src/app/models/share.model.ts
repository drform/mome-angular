export interface ShareUserInfo {
  id?: number;
  user_id?: number;
  form_id?: number;
  share_role_id?: number;
  created_at?: string;
  name?: string;
  image?: string;
}



