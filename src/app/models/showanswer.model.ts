export interface ShowAnswer {
  data?: Data[];
  creater?: Creater;
  error?: string;
}

export interface Data {
  id: number;
  order_no: number;
  data_type_id: number;
  question: string;
  answer: Answer[];
}

export interface Answer {
  user_item_id: number;
  filename?: string;
  value?: any[];
  values?: any[];
  choice_id: number;
}

export interface Creater {
  user_id: number;
  username: string;
  created_at: string;
  image: string;
}






