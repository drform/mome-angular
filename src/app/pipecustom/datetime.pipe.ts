import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'datetimeCustom' })
export class PipeDatetime implements PipeTransform {
  transform(value: string, rawDate: string): string {
    const DATE = new Date(rawDate.toString());
    return DATE.toLocaleString('th-TH');
  }
}
@Pipe({ name: 'dateCustom' })
export class PipeDate implements PipeTransform {
  transform(value: string, rawDate: string): string {
    const DATE = new Date(rawDate.toString());
    return DATE.toLocaleDateString('th-TH');
  }
}
