import { Action, State } from '@ngrx/store';
import { FormModel } from '../Models/form.model';
import * as FormActions from './../actions/form.actions';

// Section 1
const initialState: FormModel = JSON.parse(localStorage.getItem('formModel')) ? JSON.parse(localStorage.getItem('formModel')) : null;

// Section 2
export function FormReducer(state: FormModel = initialState, action: FormActions.Actions) {

  // Section 3
  switch (action.type) {
    case FormActions.ADD_FORM:
      state = action.payload;
      localStorage.setItem('formModel', JSON.stringify(state));
      return state;
    default:
      return state;
  }
}
