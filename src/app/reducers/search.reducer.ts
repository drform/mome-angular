import { Action, State } from '@ngrx/store';
import * as SearchAction from '../actions/search.actions';

// Section 1
const initialState: any = JSON.parse(localStorage.getItem('search_value')) ? JSON.parse(localStorage.getItem('search_value')) : null;

// Section 2
export function SearchReducer(state: any = initialState, action: SearchAction.Actions) {

  // Section 3
  switch (action.type) {
    case SearchAction.ADD_SEARCH:
      state = action.payload;
      localStorage.setItem('search_value', JSON.stringify(state));
      return state;

    case SearchAction.REMOVE_SEARCH:
      state = null;
      localStorage.removeItem('search_value');
      return state;

    default:
      return state;
  }
}
