import { Action, State } from '@ngrx/store';
import { User } from '../Models/user.model';
import * as UserActions from './../actions/user.actions';

// Section 1
const initialState: User = JSON.parse(localStorage.getItem('user')) ? JSON.parse(localStorage.getItem('user')) : null;

// Section 2
export function UserReducer(state: User = initialState, action: UserActions.Actions) {

  // Section 3
  switch (action.type) {
    case UserActions.ADD_USER:
      state = action.payload;
      localStorage.setItem('user', JSON.stringify(state));
      return state;

    case UserActions.REMOVE_USER:
      state = null;
      localStorage.removeItem('user');
      return state;

    default:
      return state;
  }
}
