import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MessageModel } from '../models/message.model';
import { Host } from '../../environments/environment';

const HOST_URL = Host.url;

@Injectable({
  providedIn: 'root'
})
export class AnswerService {

  msg: MessageModel;

  private options = { headers: new HttpHeaders().set('Content-Type', 'application/json') };

  constructor(private http: HttpClient) { }

  addAnswer(answer: FormData, formId) {
    return this.http.post(HOST_URL + '/forms/' + formId + '/user_forms', answer);
  }

  editAnswer(answer, answerId, formId) {
    return this.http.put(HOST_URL + '/forms/' + formId + '/user_forms/' + answerId, answer);
  }


  deleteImage(imageData: {}) {
    const promise = new Promise((resolve, reject) => {
      const img: any = {
        images: JSON.stringify(imageData)
      };
      this.http.delete(HOST_URL + '/image', { params: img }).subscribe(data => {
        this.msg = data;
        if (this.msg.message === 'Delete image Successfully.') {
          resolve(true);
        }

      });
    });
    return promise;
  }

  getFormItems(formId) {
    return this.http.get(HOST_URL + '/form_items/' + formId);
  }

  search(params, keyword) {
    return this.http.post(HOST_URL + '/user_form_items/?search=' + keyword, params);
  }
  getAnswerDetail(formId, answerid) {
    return this.http.get(HOST_URL + '/forms/' + formId + '/user_forms/' + answerid);
  }
  exportXLSX(formId, body) {
    const promise = new Promise((resolve, reject) => {
      this.http.post(HOST_URL + '/forms/' + formId + '/export', body).subscribe(data => {
        const fileXLSX: any = data;
        resolve(fileXLSX.url);
      });
    });
    return promise;
  }
}
