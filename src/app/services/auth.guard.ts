import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

import { Store } from '@ngrx/store';
import { AppState } from './../app.state';
import { User } from './../Models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  user: User;
  localUser: any;

  constructor(
    private routes: Router,
    private store: Store<AppState>
  ) {
    this.store.select('user').subscribe(data => {
      this.user = data;
    });
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (state.url === '/') {
      if (this.user !== null) {
        this.routes.navigate(['/groups']);
      }
    } else if (state.url === '/signin') {
      if (this.user !== null) {
        this.routes.navigate(['/groups']);
      }

    } else if (state.url === '/signup') {
      if (this.user !== null) {
        this.routes.navigate(['/groups']);
      }
    } else if (state.url === '/credit') {

    } else {
      if (this.user === null) {
        this.routes.navigate(['/signin']);
      }
    }
    return true;
  }

}
