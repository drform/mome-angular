import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

import { User } from './../Models/user.model';
import { Store } from '@ngrx/store';
import { AppState } from './../app.state';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  user: User;

  constructor(
    public auth: AuthService,
    private routes: Router,
    private store: Store<AppState>
  ) {
    store.select('user').subscribe(data => {
      this.user = data;
    });
  }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.user) {
      request = request.clone({
        setHeaders: {
          token: this.user.token
        },
      });
    } else if (localStorage.getItem('location') === '/signup') {

    } else if (localStorage.getItem('location') === '/forgotpassword') {

    } else if (localStorage.getItem('location') === '/forgotpassword/resetpassword') {

    } else {
      this.routes.navigate(['/']);
    }
    return next.handle(request);
  }

}
