import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host } from '../../environments/environment';
import { MessageModel } from '../models/message.model';

const HOST_URL = Host.url;

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  formData: FormData = new FormData();
  saveData: MessageModel;

  constructor(private http: HttpClient) { }

  postSignin(data) {
    const params = { session: data };
    return this.http.post(HOST_URL + '/login', params);
  }

  register(userData) {
    return this.http.post(HOST_URL + '/users', userData);
  }

  forgotEmail(emailValue) {
    const data = {
      user: {
        email: emailValue
      }
    };
    const promise = new Promise((resolve, reject) => {
      return this.http.post(HOST_URL + '/password/forgot', data).subscribe(value => {
        resolve(value);
      });
    });
    return promise;
  }

  forgotToken(tokenData) {
    const data = {
      user: {
        token: tokenData
      }
    };
    const promise = new Promise((resolve, reject) => {
      this.http.post(HOST_URL + '/password/reset', data).subscribe(value => {
        resolve(value);
      });
    });
    return promise;
  }

  forgotPassword(tokenData, passwordData) {
    const data = {
      user: {
        token: tokenData,
        password: passwordData
      }
    };
    const promise = new Promise((resolve, reject) => {
      this.http.post(HOST_URL + '/password/reset', data).subscribe(value => {
        console.log(value);
        resolve(value);
      });
    });
    return promise;
  }

  logOut() {
    return this.http.delete(HOST_URL + '/logout');
  }

}


