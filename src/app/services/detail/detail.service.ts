import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class DetailService {

  constructor() { }

  private subject = new Subject<any>();

  public update(message) {
    this.subject.next({ message: message });
  }

  public listen() {
    return this.subject.asObservable();
  }
}
