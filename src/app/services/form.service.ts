import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host } from '../../environments/environment';
import { Observable, Subject, BehaviorSubject } from 'rxjs';

const HOST_URL = Host.url;

@Injectable({
  providedIn: 'root'
})
export class FormService {

  private refreshNeed = new Subject<void>();
  public isGetAnswer: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(
    private http: HttpClient
  ) { }
  getUser(order, limit, offset) {
    return this.http.get(HOST_URL + '/users', {
      params: { order, limit, offset }
    });
  }

  getRefresh() {
    return this.refreshNeed;
  }

  getForm(param) {
    return this.http.get(HOST_URL + '/forms', { params: param });
  }
  getFormId(id) {
    return this.http.get(HOST_URL + '/forms/' + id);
  }

  getFormType() {
    return this.http.get(HOST_URL + '/data_types');
  }

  getFormItem(id) {
    return this.http.get(HOST_URL + '/form_items/' + id);
  }
  getAnswerFormAll(formId, param) {
    return this.http.get(HOST_URL + '/forms/' + formId + '/user_forms/', { params: param });
  }
  getAnswerForm(formId, answerId) {
    return this.http.get(HOST_URL + '/forms/' + formId + '/user_forms/' + answerId);
  }

  addForm(myForm) {
    return this.http.post(HOST_URL + '/forms', myForm);
  }

  deleteForm(id) {
    return this.http.delete(HOST_URL + '/forms/' + id);
  }

  deleteAnswer(formId, answerId) {
    return this.http.delete(HOST_URL + '/forms/' + formId + '/user_forms/' + answerId);
  }

  updateForm(formId, editData) {
    return this.http.put(HOST_URL + '/forms/' + formId, editData);
  }

  deleteFormItem(formData: {}) {
    const pa = ({
      form_items: JSON.stringify(formData)
    });
    return this.http.delete(HOST_URL + '/form_items', { params: pa });

  }

  exportXLSX(formId) {
    const promise = new Promise((resolve, reject) => {
      this.http.post(HOST_URL + '/forms/' + formId + '/export', '').subscribe(data => {
        const fileXLSX: any = data;
        resolve(fileXLSX.url);
      });
    });
    return promise;
  }

}


