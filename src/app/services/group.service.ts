import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Host } from '../../environments/environment';

const HOST_URL = Host.url;

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  constructor(private http: HttpClient) { }

  getGroup(param) {
    return this.http.get(HOST_URL + '/groups', { params: param });
  }

  getSelectGroup(groupId) {
    return this.http.get(HOST_URL + '/groups/' + groupId);
  }

  createGroup(groupData) {
    return this.http.post(HOST_URL + '/groups', groupData);
  }

  editGroup(groupData, groupId) {
    return this.http.patch(HOST_URL + '/groups/' + groupId, groupData);
  }

  deleteGroup(groupId) {
    return this.http.delete(HOST_URL + '/groups/' + groupId);
  }
}
