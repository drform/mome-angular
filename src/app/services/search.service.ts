import { Injectable } from '@angular/core';
import { Host } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

import { Store } from '@ngrx/store';
import { SearchState } from '../app.state';

const HOST_URL = Host.url;

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(
    private http: HttpClient,
    private store: Store<SearchState>
  ) { }

  getSearch() {
    return this.store.select('search');
  }
}
