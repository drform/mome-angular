import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Host } from '../../environments/environment';

const HOST_URL = Host.url;

@Injectable({
  providedIn: 'root'
})
export class ShareService {



  constructor(private http: HttpClient) { }

  addShareuser(user, formId) {
    const share = {
      username: user
    };
    return this.http.post(HOST_URL + '/forms/' + formId + '/shares', { share });
  }

  getShareuser(formId) {
    return this.http.get(HOST_URL + '/forms/' + formId + '/shares');
  }

  delShareuser(formId, userId) {
    return this.http.delete(HOST_URL + '/forms/' + formId + '/shares/' + userId);
  }
}
