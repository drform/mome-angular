import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host } from '../../environments/environment';

import { Store } from '@ngrx/store';
import { AppState } from '../app.state';

const HOST_URL = Host.url;

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient,
    private store: Store<AppState>
  ) { }

  getUser() {
    return this.store.select('user');
  }

  editUser(userData) {
    return this.http.put(HOST_URL + '/users', { user: userData });
  }

  searchUsers(username) {
    return this.http.get(HOST_URL + '/users/?username=' + username);
  }

  getListUser(order, limit, offset, keyword) {
    return this.http.get(HOST_URL + '/users?type=all&search=' + keyword + '&order=' + order + '&limit=' + limit + '&offset=' + offset);
  }
  getUserDetail(id) {
    return this.http.get(HOST_URL + '/users/' + id);
  }
  deleteUser(id) {
    return this.http.delete(HOST_URL + '/users/' + id);
  }
}


