import { Component, OnInit, HostListener } from '@angular/core';
import { RegisterModel } from '../../models/register.model';
import { AuthService } from '../../services/auth.service';
import { UserList, UserInfo } from '../../models/user.model';
import { UserService } from '../../services/user.service';
import { Host } from '../../../environments/environment';
import { Location } from '@angular/common';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { AlertImagesizeService } from '../../shares/alert-imagesize.service';

@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.css']
})
export class AdduserComponent implements OnInit {

  register: RegisterModel;
  confirmpassword: string;
  URL = Host.url;
  username: any = '';
  password: any = '';
  confirmPassword: any = '';
  phone: any = '';
  email: any = '';
  FullName: any = '';
  roleId: any = 2;
  roleName: any;
  //Get List
  listUser: UserList;
  keyList: any = '';
  userClicked: UserInfo;
  //Image
  imageShow = 'assets/img/default_pic.png';
  imageName = 'Choose Image';
  selectedFiles: File;
  //Get Users
  order = 'id ASC';
  limit = 5;
  offset = 0;
  keyword: any = '';
  saveData: any = [];
  // Select Image
  formImage = [
    {
      url: '/assets/img/syringe.png',
      type: 'image/png'
    }
  ];
  imageNameShow: any = 'Choose image';
  isChanged = false;
  imageValue;
  imageData: any;
  newEmail: any = null;
  newTel: any = null;

  resultNumber: any;

  isBackForm: boolean = false;
  isNextForm: boolean = true;
  stackPage = 0;

  newImageUrl = '/assets/img/default_pic.png';
  newImageShow = '/assets/img/default_pic.png';
  newImageName = 'Choose image';
  newSelectedFiles: File;

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private location: Location,
    private alertImage: AlertImagesizeService
  ) { }
  handleFileInputExam(event) {
    this.selectedFiles = event.target.files[0] as File;
    if (this.selectedFiles.size < 10 * 1024 * 1024) {
      this.imageName = this.selectedFiles.name;
      const reader = new FileReader();
      reader.onload = (evented: any) => {
        this.imageValue = evented.target.result;
        let imageText: any;
        imageText = this.imageValue.split(',')[1];
        this.imageData = imageText;
      };
      reader.readAsDataURL(this.selectedFiles);
      this.isChanged = true;
    } else {
      this.alertImage.alertImageOverSize();
    }
  }
  ngOnInit() {
    this.getUsers();
  }

  signUp() {
    if (this.password === this.confirmPassword) {
      const userData = {
        user: {
          username: this.username,
          password: this.password,
          name: 'Insert your name',
          email: this.email,
          tel: this.phone,
          position: 'Insert your position',
          role_id: this.roleId,
          image: this.newImageUrl
        }
      };
      this.saveData = [];
      Swal.fire({
        title: 'Are you sure?',
        text: 'Confirm register.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3885DE',
        cancelButtonColor: '#e41c44',
        confirmButtonText: 'Yes',
      }).then((result) => {
        if (result.value) {
          this.authService.register(userData).subscribe(data => {
            this.saveData = data;

            if (this.saveData.message === 'Sign up Complete.') {
              Swal.fire({
                title: 'Sign up Successed!',
                text: 'Your sign up successed.',
                type: 'success',
              });
              this.saveData = null;
              this.getUsers();
              this.username = '';
              this.password = '';
              this.confirmPassword = '';
              this.email = '';
              this.phone = '';
              this.roleId = '';
              this.location.path();
            } else {
              Swal.fire({
                title: 'Sign up Failed.',
                text: '' + this.saveData.message,
                html: 'Email:' + this.saveData.error.email + '<br/>' +
                  'Tel:' + this.saveData.error.tel + '<br/>' +
                  'Username:' + this.saveData.error.username,
                type: 'error',
                confirmButtonColor: '#3885DE',
                confirmButtonText: 'OK'
              });
            }
          });
        }
      });
    }
  }
  setRole(index, roleName) {
    this.roleId = index;
    this.roleName = roleName;
  }
  getUsers() {
    this.userService.getListUser(this.order, this.limit, this.offset, this.keyword).subscribe(data => {
      const len = Object.keys(data).length;
      this.resultNumber = len;
      if (len !== 0) {
        this.listUser = data;
        this.roleName = null;
        if (len >= 5) {
          this.isNextForm = true;
        } else {
          this.isNextForm = false;
        }
        if (this.stackPage === 0) {
          this.isBackForm = false;
        } else {
          this.isBackForm = true;
        }
      }
    });
  }
  backPage() {
    this.stackPage -= 1;
    this.offset -= 5;
    this.userClicked = null;
    this.getUsers();
  }
  nextPage() {
    this.isBackForm = true;
    this.stackPage += 1;
    this.offset += 5;
    this.userClicked = null;
    this.getUsers();
  }
  getDetail(index) {
    this.userService.getUserDetail(this.listUser[index].id).subscribe(data => {
      this.userClicked = data;
      this.roleName = this.userClicked.role;
      this.imageValue = this.URL + this.userClicked.image;
    });
  }

  updateProfile() {
    let userData = {
      user_id: this.userClicked.id,
      name: this.userClicked.name,
      email: this.userClicked.email,
      tel: this.userClicked.tel,
      position: this.userClicked.position,
      role_id: this.roleId,
      image: null
    };
    if (this.imageData) {
      userData.image = this.imageData;
    }
    if (this.newEmail) {
      userData.email = this.newEmail;
    }
    if (this.newTel) {
      userData.tel = this.newTel;
    }
    this.saveData = [];
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to update this!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.userService.editUser(userData).subscribe(data => {
          this.saveData = data;
          if (this.saveData.message === 'Update successfully.') {
            Swal.fire({
              title: 'Update Successed!',
              text: 'Your update successed.',
              type: 'success',
            });
            this.userClicked = null;
            this.saveData = null;
            this.location.path();
            this.getUsers();
          } else {
            Swal.fire({
              title: 'Update failed!',
              text: '' + this.saveData.error,
              type: 'error',
              confirmButtonColor: '#3885DE',
              confirmButtonText: 'OK'
            });
            this.userClicked = null;
            this.saveData = null;
            this.location.path();
            this.getUsers();
          }
        });
      } else {
        Swal.fire(
          'Cancel',
          'Cancel Success.',
          'error'
        )
      }
    });
  }
  deleteUser(id) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to delete this!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.userService.deleteUser(id).subscribe(data => {
          Swal.fire(
            'Deleted.',
            data['message'],
            'success'
          )
          this.userClicked = null;
          this.getUsers();
          this.location.path();
        });
      } else {
        Swal.fire(
          'Cancel',
          'Cancel Success.',
          'error'
        )
      }
    });
  }
  handleFileInput(event) {
    this.formImage = [];
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < event.target.files.length; i++) {
      this.selectedFiles = <File>event.target.files[i];
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.formImage.push({
          url: event.target.result,
          type: this.selectedFiles.type
        });
        this.imageName = this.selectedFiles.name;
      }
      reader.readAsDataURL(this.selectedFiles);
    }
  }

  NewEmail() {
    this.newEmail = this.userClicked.email;
  }
  NewTel() {
    this.newTel = this.userClicked.tel;
  }

  handleSelectImage(event) {
    this.newSelectedFiles = event.target.files[0] as File;
    if (this.newSelectedFiles) {
      if (this.newSelectedFiles.size < 10 * 1024 * 1024) {
        const reader = new FileReader();
        reader.onload = (evented: any) => {
          this.newImageShow = evented.target.result;
          this.newImageName = this.newSelectedFiles.name;
          this.newImageUrl = this.newImageShow.split(',')[1];
        };
        reader.readAsDataURL(this.newSelectedFiles);
      } else {
        this.alertImage.alertImageOverSize();
        this.newImageName = 'Choose image';
        this.newImageShow = '/assets/img/default_pic.png';
      }
    }
  }

  @HostListener('window:keyup', ['$event'])
  onKeyUp($event: KeyboardEvent) {
    console.log($event.keyCode);
    if (!(($event.keyCode >= 48 && $event.keyCode <= 57) || !($event.keyCode >= 96 && $event.keyCode <= 105)) && this.userClicked) {
      this.newTel = '';
      this.userClicked.tel = '';
      this.phone = '';
    }
  }
}
