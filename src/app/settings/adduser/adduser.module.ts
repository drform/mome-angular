import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdduserRoutingModule } from './adduser-routing.module';
import { AdduserComponent } from '../adduser/adduser.component';
import { DemoMaterialModule } from '../../material-module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AdduserComponent
  ],
  imports: [
    CommonModule,
    AdduserRoutingModule,
    FormsModule,
    DemoMaterialModule
  ]
})
export class AdduserModule { }
