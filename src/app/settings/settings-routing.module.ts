import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SettingsComponent } from '../settings/settings.component';

const routes: Routes = [
  {
    path: '',
    component: SettingsComponent,
    children: [
      {
        path: 'createuser',
        loadChildren: '../settings/adduser/adduser.module#AdduserModule'
      },
      {
        path: 'account',
        loadChildren: '../settings/user/user.module#UserModule'
      }, {
        path: 'credit',
        loadChildren: '../settings/credit/credit.module#CreditModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
