import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule } from './settings-routing.module';
import { FormsModule } from '@angular/forms';
import { LayoutModule } from '../Layout/layout.module';
import { SettingsComponent } from '../settings/settings.component';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { DemoMaterialModule } from '../material-module';

@NgModule({
  imports: [
    CommonModule,
    SettingsRoutingModule,
    LayoutModule,
    FormsModule,
    DemoMaterialModule,
    SweetAlert2Module.forRoot()
  ],
  declarations: [
    SettingsComponent,
  ],
})
export class SettingsModule { }
