import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Store } from '@ngrx/store';
import { AppState } from '../../app.state';
import * as UserActions from '../../actions/user.actions';

import { UserService } from '../../services/user.service';
import { User } from '../../models/user.model';

import { Host } from '../../../environments/environment';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { AlertImagesizeService } from '../../shares/alert-imagesize.service';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {


  HOST_URL = Host.url;
  user: User;

  nameValue;
  emailValue;
  telValue;
  positionValue;
  imageValue;

  oldPassword;
  newPassword;
  confirmPassword;

  saveData = [];
  imageData: any;

  // image
  imageShow: any;
  selectedFiles: File;
  isChanged = false;

  constructor(
    private store: Store<AppState>,
    private userservice: UserService,
    private alertImage: AlertImagesizeService
  ) { }

  ngOnInit() {
    this.getUser();
  }

  getUser() {
    this.userservice.getUser().subscribe(state => {
      if (state !== null) {
        this.user = state;
        this.nameValue = this.user.user_info.name;
        this.emailValue = this.user.user_info.email;
        this.telValue = this.user.user_info.tel;
        this.positionValue = this.user.user_info.position;
        this.imageValue = this.HOST_URL + this.user.user_info.image;
      }

    });
  }



  editProfile() {
    const userInfo = {
      name: this.nameValue,
      email: this.emailValue,
      tel: this.telValue,
      position: this.positionValue,
      image: this.imageData
    };

    Swal.fire({
      title: 'Are you sure edit user?',
      text: 'Confirm edit this user.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3885DE',
      cancelButtonColor: '#e41c44',
      confirmButtonText: 'Yes',
    }).then((result) => {
      if (result.value) {
        this.userservice.editUser(userInfo).subscribe(data => {

          this.saveData.push(data);
          const checkData: any = data;

          if (checkData.error) {
            Swal.fire({
              title: 'Something wrong!',
              text: 'Can not edit this user',
              type: 'error',
              confirmButtonColor: '#3885DE',
              confirmButtonText: 'OK'
            });
          } else {
            Swal.fire({
              title: 'Edit Successed!',
              text: 'Your edit this user successed.',
              type: 'success',
            });

            this.saveData = [];

            this.user.user_info = checkData.user_info;
            this.store.dispatch(new UserActions.AddUser(this.user));
            this.getUser();
          }


        }, (error) => {
          Swal.fire({
            title: 'Something wrong!',
            text: 'Can not edit this user',
            type: 'error',
            confirmButtonColor: '#3885DE',
            confirmButtonText: 'OK'
          });
        });
      }
    });


  }

  changePassword() {
    if (this.newPassword === this.confirmPassword) {
      const user = {
        password: this.oldPassword,
        password_new: this.newPassword
      };

      Swal.fire({
        title: 'Are you sure change password?',
        text: 'Confirm change this password.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3885DE',
        cancelButtonColor: '#e41c44',
        confirmButtonText: 'Yes',
      }).then((result) => {
        if (result.value) {
          this.userservice.editUser(user).subscribe(data => {
            this.saveData.push(data);
            const checkData: any = data;


            if (checkData.error) {
              Swal.fire({
                title: 'Something wrong!',
                text: 'Can not change this password',
                type: 'error',
                confirmButtonColor: '#3885DE',
                confirmButtonText: 'OK'
              });
            } else {
              Swal.fire({
                title: 'Change Password Successed!',
                text: 'Your change this password successed.',
                type: 'success',
              });

              this.saveData = [];

              this.getUser();
            }


          }, (error) => {
            Swal.fire({
              title: 'Something wrong!',
              text: 'Can not change this password',
              type: 'error',
              confirmButtonColor: '#3885DE',
              confirmButtonText: 'OK'
            });
          });
        }
      });
    }

  }

  handleFileInputExam(event) {
    this.selectedFiles = event.target.files[0] as File;
    if (this.selectedFiles.size < 10 * 1024 * 1024) {
      const reader = new FileReader();
      reader.onload = (evented: any) => {
        this.imageValue = evented.target.result;
        let imageText: any;
        imageText = this.imageValue.split(',')[1];
        this.imageData = imageText;
      };
      reader.readAsDataURL(this.selectedFiles);
      this.isChanged = true;
    } else {
      this.alertImage.alertImageOverSize();
    }

  }

  changeImage() {
    const userInfo = {
      image: this.imageData
    };

    Swal.fire({
      title: 'Are you sure change image?',
      text: 'Confirm change this image.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3885DE',
      cancelButtonColor: '#e41c44',
      confirmButtonText: 'Yes',
    }).then((result) => {
      if (result.value) {
        this.userservice.editUser(userInfo).subscribe(data => {
          this.saveData.push(data);
          const checkData: any = data;


          if (checkData.error) {
            Swal.fire({
              title: 'Something wrong!',
              text: 'Can not change this image',
              type: 'error',
              confirmButtonColor: '#3885DE',
              confirmButtonText: 'OK'
            });
          } else {
            Swal.fire({
              title: 'Change image Successed!',
              text: 'Your change this image successed.',
              type: 'success',
            });

            this.saveData = [];

            this.user.user_info = checkData.user_info;
            this.store.dispatch(new UserActions.AddUser(this.user));
            this.getUser();
          }


        }, (error) => {
          Swal.fire({
            title: 'Something wrong!',
            text: 'Can not change this image',
            type: 'error',
            confirmButtonColor: '#3885DE',
            confirmButtonText: 'OK'
          });
        });
      }
    });

  }
}
