import { Injectable } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';
Injectable({
  providedIn: 'root'
})
export class AlertImageService {

  saveData: any = [];

  constructor() { }

  alertImageOverSize() {
    Swal.fire({
      title: 'Sorry,Over size file',
      text: 'Maximum file size is 2 Mb.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3885DE',
      confirmButtonText: 'Yes',
    });
  }
}
