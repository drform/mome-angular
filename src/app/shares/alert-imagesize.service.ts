import { Injectable } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Injectable({
  providedIn: 'root'
})
export class AlertImagesizeService {

  constructor() { }

  alertImageOverSize() {
    Swal.fire({
      title: 'Sorry,Over size file',
      text: 'Maximum file size is 2 Mb.',
      type: 'warning',
      confirmButtonColor: '#3885DE',
      confirmButtonText: 'Ok',
    });
  }
}
