import { Injectable } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

import { Store } from '@ngrx/store';
import { AppState } from '../app.state';
import * as UserActions from '../actions/user.actions';

@Injectable({
  providedIn: 'root'
})
export class AlertLogoutService {

  saveData: any = [];

  constructor(
    private routes: Router,
    private authService: AuthService,
    private store: Store<AppState>,
  ) { }

  alertLogout() {
    this.saveData = [];
    const promise = new Promise((resolve, reject) => {
      Swal.fire({
        title: 'Logout?',
        text: 'Confirm logout.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3885DE',
        cancelButtonColor: '#e41c44',
        confirmButtonText: 'Yes',
      }).then((result) => {
        if (result.value) {
          resolve(result.value);
          this.authService.logOut().subscribe(data => {
            this.store.dispatch(new UserActions.RemoveUser());
          });
          this.routes.navigate(['/']);
        }
      });
    });
    return promise;

  }
}
