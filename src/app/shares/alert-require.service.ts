import { Injectable } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AlertRequireService {

  constructor(
    private routes: Router,
  ) { }

  alertRequire() {
    Swal.fire({
      title: 'Some text field is empty.',
      text: 'please insert this field and submit again',
      type: 'error',
      confirmButtonColor: '#3885DE',
      confirmButtonText: 'OK'
    });
  }

}
