import { Injectable } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Injectable({
  providedIn: 'root'
})
export class AlertResetpasswordService {

  constructor() { }

  passwordReset() {
    const promise = new Promise((resolve, reject) => {
      Swal.fire({
        title: 'Password is Reset!',
        type: 'success',
        confirmButtonColor: '#3885DE',
        confirmButtonText: 'OK'
      });
      resolve(true);
    });
    return promise;
  }




}
