import { Injectable } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { AuthService } from '../services/auth.service';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';

@Injectable({
  providedIn: 'root'
})
export class AlertUserService {

  saveData: any = [];

  constructor(
    private routes: Router,
    private authService: AuthService,
    private location: Location,
    private userService: UserService,
  ) { }

  alertRegister(userData) {
    this.saveData = [];
    Swal.fire({
      title: 'Are you sure?',
      text: 'Confirm register.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3885DE',
      cancelButtonColor: '#e41c44',
      confirmButtonText: 'Yes',
    }).then((result) => {
      if (result.value) {
        this.authService.register(userData).subscribe(data => {
          this.saveData = data;

          if (this.saveData.message === 'Sign up Complete.') {
            Swal.fire({
              title: 'Sign up Successed!',
              text: 'Your sign up successed.',
              type: 'success',
            });
            this.saveData = null;
          } else {
            Swal.fire({
              title: 'Sign up Failed.',
              text: '' + this.saveData.message,
              html: 'Email:' + this.saveData.error.email + '<br/>' +
                'Tel:' + this.saveData.error.tel + '<br/>' +
                'Username:' + this.saveData.error.username,
              type: 'error',
              confirmButtonColor: '#3885DE',
              confirmButtonText: 'OK'
            });
          }
        });
      }
    });
  }

  alertUpdateProfile(userData) {
    this.saveData = [];
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to update this!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.userService.editUser(userData).subscribe(data => {
          this.saveData = data;

          if (this.saveData.message === 'Update successfully.') {
            Swal.fire({
              title: 'Update Successed!',
              text: 'Your update successed.',
              type: 'success',
            });
            this.saveData = null;
            this.location.path();
          } else {
            Swal.fire({
              title: 'Update failed!',
              text: '' + this.saveData.message,
              html: 'Email:' + this.saveData.error.email + '<br/>' +
                'Tel:' + this.saveData.error.tel + '<br/>' +
                'Username:' + this.saveData.error.username,
              type: 'error',
              confirmButtonColor: '#3885DE',
              confirmButtonText: 'OK'
            });
          }
        });
      } else {
        Swal.fire(
          'Cancel',
          'Cancel Success.',
          'error'
        )
      }
    });
  }

}
