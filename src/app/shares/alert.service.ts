import { Injectable } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { Router } from '@angular/router';

import { AnswerService } from '../services/answer.service';
import { FormService } from '../services/form.service';
import { ShareService } from '../services/share.service';
import { AuthService } from '../services/auth.service';

import { ReturnData } from '../models/form.model';
import { Location } from '@angular/common';
import { MessageModel } from '../models/message.model';
@Injectable({
  providedIn: 'root'
})
export class AlertService {

  saveData: MessageModel;


  constructor(
    private routes: Router,
    private answerService: AnswerService,
    private formService: FormService,
    private shareService: ShareService,
    private authService: AuthService,
    private location: Location,
  ) { }

  alertLogin() {
    Swal.fire({
      title: 'Something wrong!',
      text: 'username or password is wrong.',
      type: 'error',
      confirmButtonColor: '#3885DE',
      confirmButtonText: 'OK'
    });
  }

  alertLoginNull() {
    Swal.fire({
      title: 'Username or password is empty!',
      text: 'please fill username or password and try again.',
      type: 'error',
      confirmButtonColor: '#3885DE',
      confirmButtonText: 'OK'
    });
  }

  alertMakeQuestion(answer, groupId, formId) {
    const promise = new Promise((resolve, reject) => {
      Swal.fire({
        title: 'Are you sure?',
        text: 'Confirm create this Questions.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3885DE',
        cancelButtonColor: '#e41c44',
        confirmButtonText: 'Yes',
      }).then((result) => {
        if (result.value) {
          this.answerService.addAnswer(answer, formId).subscribe(data => {
            this.saveData = data;
            resolve(result.value);
            if (this.saveData.message === 'Save successfully.') {
              Swal.fire({
                title: 'Successed!',
                text: 'Your make a question successed.',
                type: 'success',
              });
              this.routes.navigate(['/../../../groups/' + groupId + '/show/' + formId]);
            } else if (this.saveData.error === 'Don\'t have user_form_item') {
              Swal.fire({
                title: 'Answer is not create',
                text: 'Success but can\'t create answer',
                type: 'success',
              });
            }


          });
        } else {
          resolve(result.value);
        }
      });
    });
    return promise;
  }

  alertMakeQuestionRequire() {
    Swal.fire({
      title: 'Something wrong!',
      text: 'look like some textbox is empty, please input and retry again.',
      type: 'error',
      confirmButtonColor: '#3885DE',
      confirmButtonText: 'OK'
    });
  }

  alertDeleteForm(formId, groupId) {
    this.saveData = null;
    Swal.fire({
      title: 'Are you sure Delete form?',
      text: 'Confirm delete this form.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3885DE',
      cancelButtonColor: '#e41c44',
      confirmButtonText: 'Yes',
    }).then((result) => {
      if (result.value) {
        this.formService.deleteForm(formId).subscribe(data => {
          this.saveData = data;

          if (this.saveData.message === 'Delete Successfully.') {
            Swal.fire({
              title: 'Delete Successed!',
              text: 'Your delete this form successed.',
              type: 'success',
            });

            this.saveData = null;
            this.routes.navigate(['../groups/' + groupId]);
          } else {
            Swal.fire({
              title: 'Something wrong!',
              text: 'Can not delete this form',
              type: 'error',
              confirmButtonColor: '#3885DE',
              confirmButtonText: 'OK'
            });
          }

        });
      }
    });

  }





  alertDeleteAnswer(formId, groupId, ansId) {
    this.saveData = null;
    const promise = new Promise((resolve, reject) => {
      Swal.fire({
        title: 'Are you sure Delete?',
        text: 'Confirm delete this answer.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3885DE',
        cancelButtonColor: '#e41c44',
        confirmButtonText: 'Yes',
      }).then((result) => {
        if (result.value) {
          resolve(result);
        }
      });
    });
    return promise;
  }

  testAlert() {
    const promise = new Promise((resolve, reject) => {
      Swal.fire({
        title: 'Confirm Create form?',
        text: 'Confirm create this form.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3885DE',
        cancelButtonColor: '#e41c44',
        confirmButtonText: 'Yes',
      }).then((result) => {
        if (result.value) {
          resolve(result);
        }
      });
    });
    return promise;
  }

  alertEmptyCreateForm() {
    Swal.fire({
      title: 'Question field is empty!',
      text: 'Please fillable title, description and select type of questionaire.',
      type: 'error',
      confirmButtonColor: '#3885DE',
      confirmButtonText: 'OK'
    });
  }

  alertCreateForm(myForm, groupId) {
    const promise = new Promise((resolve, reject) => {
      Swal.fire({
        title: 'Confirm Create form?',
        text: 'Confirm create this form.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3885DE',
        cancelButtonColor: '#e41c44',
        confirmButtonText: 'Yes',
      }).then((result) => {
        resolve(result);
        if (result.value) {
          this.formService.addForm(myForm).subscribe(data => {
            this.saveData = data;

            if (this.saveData.message === 'Save form successfully.') {
              Swal.fire({
                title: 'Create Form Successed!',
                text: 'Your create form successed.',
                type: 'success',
              });
              this.saveData = null;
              this.routes.navigate(['../groups/' + groupId]);
            } else if (this.saveData.error.title) {
              if (this.saveData.error.title[0] === 'can\'t be blank') {
                Swal.fire({
                  title: 'Title is Empty!',
                  text: 'Can not create form.',
                  type: 'error',
                  confirmButtonColor: '#3885DE',
                  confirmButtonText: 'OK'
                });
              } else if (this.saveData.error.title[0] === 'has already been taken') {
                Swal.fire({
                  title: 'Title has already been taken',
                  text: 'Can not create form.',
                  type: 'error',
                  confirmButtonColor: '#3885DE',
                  confirmButtonText: 'OK'
                });
              }
            } else if (this.saveData.error.description) {

              if (this.saveData.error.description[0] === 'can\'t be blank') {
                Swal.fire({
                  title: 'Description is Empty!',
                  text: 'Can not create form.',
                  type: 'error',
                  confirmButtonColor: '#3885DE',
                  confirmButtonText: 'OK'
                });
              }
            } else {
              Swal.fire({
                title: 'Something wrong!',
                text: 'Can not edit this form',
                type: 'error',
                confirmButtonColor: '#3885DE',
                confirmButtonText: 'OK'
              });
            }
          });
        }
      });
    });
    return promise;
  }

  alertEditForm(formId, addForm, groupId, itemDelete) {
    this.saveData = null;
    const promise = new Promise((resolve, reject) => {
      Swal.fire({
        title: 'Confirm edit form?',
        text: 'Confirm edit this form.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3885DE',
        cancelButtonColor: '#e41c44',
        confirmButtonText: 'Yes',
      }).then((result) => {
        if (result.value) {
          if (itemDelete.length > 0) {
            this.formItemDelete(itemDelete).then(data => {

              this.updateForm(formId, addForm, groupId).then(dataValue => {
                resolve(result.value);
              });
            });
          } else {
            this.updateForm(formId, addForm, groupId).then(dataValue => {
              resolve(result.value);
            });
          }

        } else {
          resolve(result.value);
        }
      });
    });
    return promise;
  }

  formItemDelete(itemDelete) {
    const promise = new Promise((resolve, reject) => {
      this.formService.deleteFormItem(itemDelete).subscribe(data => {
        resolve(data);
      });
    });
    return promise;
  }

  updateForm(formId, addForm, groupId) {
    const promise = new Promise((resolve, reject) => {
      this.formService.updateForm(formId, addForm).subscribe(data => {
        this.saveData = data;

        if (this.saveData.message === 'Updated Successfully.') {
          Swal.fire({
            title: 'Edit form Successed!',
            text: 'Your edit form successed.',
            type: 'success',
          });

          this.saveData = null;
          resolve(true);
          this.routes.navigate(['/../../../groups/' + groupId + '/show/' + formId]);
        } else if (this.saveData.error.title) {
          if (this.saveData.error.title[0] === 'can\'t be blank') {
            Swal.fire({
              title: 'Title is Empty!',
              text: 'Can not edit this form',
              type: 'error',
              confirmButtonColor: '#3885DE',
              confirmButtonText: 'OK'
            });
            resolve(true);
          } else if (this.saveData.error.title[0] === 'has already been taken') {
            Swal.fire({
              title: 'Title has already been taken',
              text: 'Can not edit this form',
              type: 'error',
              confirmButtonColor: '#3885DE',
              confirmButtonText: 'OK'
            });
            resolve(true);
          }

        } else if (this.saveData.error.description) {
          if (this.saveData.error.description[0] === 'can\'t be blank') {
            Swal.fire({
              title: 'Description is Empty!',
              text: 'Can not edit this form',
              type: 'error',
              confirmButtonColor: '#3885DE',
              confirmButtonText: 'OK'
            });
            resolve(true);
          }

        } else {
          Swal.fire({
            title: 'Something wrong!',
            text: 'Can not edit this form',
            type: 'error',
            confirmButtonColor: '#3885DE',
            confirmButtonText: 'OK'
          });
          resolve(true);
        }

      });
    });
    return promise;
  }

  alertNull() {
    Swal.fire({
      title: 'Dropdown or Checkbox is empty!',
      text: 'please insert value in dropdown ir checkbox and try again.',
      type: 'error',
      confirmButtonColor: '#3885DE',
      confirmButtonText: 'OK'
    });
  }

}
