import { Injectable } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { Router } from '@angular/router';

import { AnswerService } from '../services/answer.service';

@Injectable({
  providedIn: 'root'
})
export class AlerteditanswerService {

  constructor(
    private routes: Router,
    private answerService: AnswerService
  ) { }

  alertEditQuestionRequire() {
    Swal.fire({
      title: 'Something wrong!',
      text: 'look like some textbox is empty, please input and retry again.',
      type: 'error',
      confirmButtonColor: '#3885DE',
      confirmButtonText: 'OK'
    });
  }

  alertEditQuestion(answer, answerId, groupId, formId) {
    const promise = new Promise((resolve, reject) => {
      Swal.fire({
        title: 'Are you sure?',
        text: 'Confirm edit this Questions.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3885DE',
        cancelButtonColor: '#e41c44',
        confirmButtonText: 'Yes',
      }).then((result) => {
        if (result.value) {

          this.answerService.editAnswer(answer, answerId, formId).subscribe(data => {
            Swal.fire({
              title: 'Successed!',
              text: 'Your make a question successed.',
              type: 'success',
            });

            resolve(result.value);
          });
        }
      });
    });
    return promise;
  }

}
