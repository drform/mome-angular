import { Injectable } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { Router } from '@angular/router';

import { GroupService } from '../services/group.service';

@Injectable({
  providedIn: 'root'
})
export class AlertgroupService {

  saveData: any = [];

  constructor(private groupService: GroupService, private routes: Router) { }

  alertCreateGroup(groupData) {
    this.saveData = [];
    Swal.fire({
      title: 'Confirm Create Group?',
      text: 'Confirm create this group.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3885DE',
      cancelButtonColor: '#e41c44',
      confirmButtonText: 'Yes',
    }).then((result) => {
      if (result.value) {
        this.groupService.createGroup(groupData).subscribe(data => {
          this.saveData = data;

          if (this.saveData.message === 'Save group successfully.') {
            Swal.fire({
              title: 'Create group Successed!',
              text: 'Your create group successed.',
              type: 'success',
            });

            this.saveData = null;
            this.routes.navigate(['../groups']);
          } else {
            Swal.fire({
              title: 'Something wrong!',
              text: 'Can not create this group',
              type: 'error',
              confirmButtonColor: '#3885DE',
              confirmButtonText: 'OK'
            });
          }

        });
      }
    });
  }

  alertGroupEmpty() {
    Swal.fire({
      title: 'Title is empty!',
      text: 'Please insert title and try again.',
      type: 'error',
      confirmButtonColor: '#3885DE',
      confirmButtonText: 'OK'
    });
  }

  alertDeleteGroup(groupId) {
    this.saveData = [];
    Swal.fire({
      title: 'Confirm Delete Group?',
      text: 'Confirm delete this group.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3885DE',
      cancelButtonColor: '#e41c44',
      confirmButtonText: 'Yes',
    }).then((result) => {
      if (result.value) {
        this.groupService.deleteGroup(groupId).subscribe(data => {
          this.saveData = data;

          if (this.saveData.message === 'Delete Successfully') {
            Swal.fire({
              title: 'Delete group Successed!',
              text: 'Your delete group successed.',
              type: 'success',
            });
            this.saveData = null;

          } else {
            Swal.fire({
              title: 'Something wrong!',
              text: 'Can not delete this group',
              type: 'error',
              confirmButtonColor: '#3885DE',
              confirmButtonText: 'OK'
            });
          }

        });
      }
    });
  }
  alertEditGroup(groupData, groupId) {
    this.saveData = [];
    Swal.fire({
      title: 'Confirm Edit Group?',
      text: 'Confirm edit this group.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3885DE',
      cancelButtonColor: '#e41c44',
      confirmButtonText: 'Yes',
    }).then((result) => {
      if (result.value) {
        this.groupService.editGroup(groupData, groupId).subscribe(data => {
          this.saveData = data;

          if (this.saveData.message === 'Update group successfully.') {
            Swal.fire({
              title: 'Edit group Successed!',
              text: 'Your edit group successed.',
              type: 'success',
            });
            this.saveData = null;
          } else {
            Swal.fire({
              title: 'Something wrong!',
              text: 'Can not edit this group',
              type: 'error',
              confirmButtonColor: '#3885DE',
              confirmButtonText: 'OK'
            });
          }

        });
      }
    });
  }
}
